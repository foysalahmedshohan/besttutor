
@extends('layout.master')
@section('content')

<section id="main" class="clearfix  ad-profile-page">
<div class="container ad-profile section">
<div class="user-profile">
<div class="user-images">
<img src="{{asset('assets/images/user.jpg')}}" alt="User Images" class="img-fluid">
</div>
<div class="user">
<h2>Hello, <a href="#">Sir</a></h2>
<h5>You last logged in at: 14-01-2018 6:40 AM [ USA time (GMT + 6:00hrs)]</h5>
</div>
<div class="favorites-user">

<div class="favorites">
<a href="#">18<small>Match</small></a>
</div>
<div class="favorites">
<a href="#">1158<small>Total Ads</small></a>
</div>
</div>
</div>
<ul class="user-menu">
<li class="active"><a href="{{route('tutorProfile.index')}}">Home</a></li>
<li><a href="{{route('tutorProfile.postList')}}">Job Post</a></li>
<li><a href="">Close account</a></li>
<li><a href="{{route('tutorProfile.create')}}"><span>Profile</span></a></li>
<li> <a class="" href="{{ route('logout') }}"
         onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">Logout                            
     </a>
         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
         @csrf
        </form>
 </li>

</ul>
</div>
</section>@yield('tutor_content')
@endsection
