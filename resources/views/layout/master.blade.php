<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Theme Region">
<meta name="description" content="">
<title>BEST TUTOR</title>

<link rel="stylesheet" href=" {{ asset('assets/css/bootstrap.min.css') }}" >
<link rel="stylesheet" href=" {{ asset('assets/css/font-awesome.min.css') }}">
<link rel="stylesheet" href=" {{ asset('assets/css/icofont.css') }}  ">
<link rel="stylesheet" href=" {{ asset('assets/css/owl.carousel.css') }} ">
<link rel="stylesheet" href=" {{ asset('assets/css/slidr.css') }} ">
<link rel="stylesheet" href=" {{ asset('assets/css/main.css') }}  ">
<link id="preset" rel="stylesheet" href=" {{ asset('assets/css/presets/preset1.css') }}   ">
<link rel="stylesheet" href="  {{ asset('assets/css/responsive.css') }}  ">

<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

<link rel="icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href=" {{ asset('assets/images/ico/apple-touch-icon-144-precomposed.png') }}   ">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href=" {{ asset('assets/images/ico/apple-touch-icon-114-precomposed.png') }}  ">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href=" {{ asset('assets/ images/ico/apple-touch-icon-72-precomposed.html') }} ">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href=" {{ asset('assets/images/ico/apple-touch-icon-57-precomposed.png') }}  ">
<script src=" {{ asset('assets/js/jquery.min.js') }} " type="c959cb70ab44b57ce80291a9-text/javascript"></script>



<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
  <header id="header" class="clearfix">

<nav class="navbar navbar-default navbar-expand-lg">
<div class="container">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#tr-mainmenu" aria-controls="tr-mainmenu" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"><i class="fa fa-align-justify"></i></span>
</button>
<a class="navbar-brand" href="{{route('dashboard')}}"><img class="img-fluid" src="  {{ asset('assets/images/logo.png') }} " height="80px" width="130" alt="Logo"></a>
<div class="collapse navbar-collapse" id="tr-mainmenu">
<ul class="nav navbar-nav">
<!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Home <span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="index-2.html">Home Default </a></li>
<li><a href="index-one.html">Home Version-1</a></li>
<li><a href="index-two.html">Home Version-2</a></li>
<li><a href="index-three.html">Home Version-3</a></li>
<li><a href="index-car.html">Home Car-1<span class="badge">New</span></a></li>
<li><a href="index-car-two.html">Home Car-2<span class="badge">New</span></a></li>
<li><a href="directory.html">Home Directory<span class="badge">Latest</span></a></li>
</ul>
</li> -->
<li><a href="{{route('dashboard')}}">Home</a></li>
<!-- <li><a href="categories.html">Category</a></li> -->
<li><a href="{{route('index')}}">Find a Student</a></li>
<li><a href="{{route('create')}}">Find a Tutor</a></li>


<li class="active dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
<ul class="dropdown-menu">
<!-- <li><a href="about-us.html">ABout Us</a></li> -->
<li><a href="{{route('contact')}}">Contact Us</a></li>
<!-- <li><a href="ad-post.html">Ad post</a></li>
<li><a href="ad-post-details.html">Ad post Details</a></li>
<li><a href="categories-main.html">Category Ads</a></li>
<li><a href="details.html">Ad Details</a></li>
<li><a href="my-ads.html">My Ads</a></li>
<li><a href="my-profile.html">My Profile</a></li>
<li><a href="favourite-ads.html">Favourite Ads</a></li>
<li><a href="archived-ads.html">Archived Ads</a></li>
<li><a href="pending-ads.html">Pending Ads</a></li>
<li><a href="delete-account.html">Close Account</a></li>
<li><a href="published.html">Ad Publised</a></li>
<li><a href="signup.html">Sign Up</a></li>
<li class="active"><a href="signin.html">Sign In</a></li>
<li><a href="faq.html">FAQ</a></li>
<li><a href="coming-soon.html">Coming Soon <span class="badge">New</span></a></li>
<li><a href="pricing.html">Pricing<span class="badge">New</span></a></li>
<li><a href="500-page.html">500 Opsss<span class="badge">New</span></a></li>
<li><a href="404-page.html">404 Error<span class="badge">New</span></a></li> -->
</ul>
</li>

<li><a href="{{route('login')}}"><i class="fa fa-user"></i> Sign In / Register</a></li>
</ul>
</div>

<div class="nav-right">
<!-- <div class="dropdown language-dropdown">
<i class="fa fa-globe"></i>
<a data-toggle="dropdown" href="#"><span class="change-text">United Kingdom</span> <i class="fa fa-angle-down"></i></a>
<ul class="dropdown-menu language-change">
<li><a href="#">United Kingdom</a></li>
<li><a href="#">United States</a></li>
<li><a href="#">China</a></li>
<li><a href="#">Russia</a></li>
</ul>
</div> -->

<!-- <ul class="sign-in">
<li><i class="fa fa-user"></i></li>
<li><a href="signin.html"> Sign In </a></li>
<li><a href="signup.html">Register</a></li>
</ul> -->
<a href="{{route('register')}}" class="btn">Become a Tutor</a>
</div>
</div>
</nav>
</header>

   
  @yield('content')

 <footer id="footer" class="clearfix">

<section class="footer-top clearfix">
<div class="container">
<div class="row">

<div class="col-sm-6 col-md-3">
<div class="footer-widget">
<h3>Best Tutors</h3>
<ul>
<li><a href="#">About Us</a></li>
<li><a href="#">Contact Us</a></li>
<li><a href="#">Careers</a></li>
<li><a href="#">All Cities</a></li>
<li><a href="#">Help & Support</a></li>
<li><a href="#">Advertise With Us</a></li>
<li><a href="#">Blog</a></li>
</ul>
</div>
</div>

<div class="col-sm-6 col-md-3">
<div class="footer-widget">
<h3>Usefull Links</h3>
<ul>
<li><a href="#">How to sell fast</a></li>
<li><a href="#">Membership</a></li>
<li><a href="#">Banner Advertising</a></li>
<li><a href="#">Promote your ad</a></li>
<li><a href="#">Trade Delivers</a></li>
<li><a href="#">FAQ</a></li>
</ul>
</div>
</div>

<div class="col-sm-6 col-md-3">
<div class="footer-widget social-widget">
<h3>Follow us on</h3>
<ul>
<li><a href="#"><i class="fa fa-facebook-official"></i>Facebook</a></li>
<li><a href="#"><i class="fa fa-twitter-square"></i>Twitter</a></li>
<li><a href="#"><i class="fa fa-google-plus-square"></i>Google+</a></li>
<li><a href="#"><i class="fa fa-youtube-play"></i>youtube</a></li>
</ul>
</div>
</div>

<div class="col-sm-6 col-md-3">
<div class="footer-widget news-letter">
<h3>Newsletter</h3>
<p>Trade is Worldest leading classifieds platform that brings!</p>

<form action="#">
<input type="email" class="form-control" placeholder="Your email id">
<button type="submit" class="btn btn-primary">Sign Up</button>
</form>
</div>
</div>
</div>
</div>
</section>
<div class="footer-bottom clearfix text-center">
<div class="container">
<p>Copyright &copy; 2020. Developed by <a href="http://artificial-soft.com/">Artificial Soft</a></p>
</div>
</div>
</footer>




<script src=" {{ asset('assets/js/popper.min.js') }} " type="c959cb70ab44b57ce80291a9-text/javascript"></script>
<script src=" {{ asset('assets/js/modernizr.min.js') }} " type="c959cb70ab44b57ce80291a9-text/javascript"></script>
<script src=" {{ asset('assets/js/bootstrap.min.js') }} " type="c959cb70ab44b57ce80291a9-text/javascript"></script>
<script src="https://maps.google.com/maps/api/js?sensor=true" type="c959cb70ab44b57ce80291a9-text/javascript"></script>
<script src=" {{ asset('assets/js/gmaps.min.js') }} " type="c959cb70ab44b57ce80291a9-text/javascript"></script>
<script src=" {{ asset('assets/js/owl.carousel.min.js') }} " type="c959cb70ab44b57ce80291a9-text/javascript"></script>
<script src=" {{ asset('assets/js/scrollup.min.js') }} " type="c959cb70ab44b57ce80291a9-text/javascript"></script>
<script src=" {{ asset('assets/js/price-range.js') }} " type="c959cb70ab44b57ce80291a9-text/javascript"></script>
<script src=" {{ asset('assets/js/jquery.countdown.js') }} " type="c959cb70ab44b57ce80291a9-text/javascript"></script>
<script src=" {{ asset('assets/js/custom.js') }} " type="c959cb70ab44b57ce80291a9-text/javascript"></script>
<script src=" {{ asset('ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js') }} " data-cf-settings="c959cb70ab44b57ce80291a9-|49" defer=""></script>

</body>
</html>