
@extends('admin-layout.master')
@section('content')

<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Subjects</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Best Tutor</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Add All Subjects Name</li>
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="javascript:void(0);" class="btn btn-sm btn-primary" title="">Dashboard</a>
                        <a href="#" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a>
                    </div>
                </div>
            </div>
    <form role="form" action="{{route('subject.store')}}" method="POST" enctype="multipart/form-data">
     @csrf
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <div class="row">

                                      @if ($errors->any())
                                            <div class="alert alert-warning alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                @if ($errors->count() == 1)
                                                   {{$errors->first()}}
                                                @else
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </div>
                                    @endif


                                <div class="row col-lg-12">
                                    <div class="col-lg-3">
                                    <label for="basic-url">Add Subjects Name:</label>
                                    </div>
                                    <div class="col-lg-7">
                                    <div class="form-group">                                   
                                        <input type="text" name="name" class="form-control" placeholder="Example: Math">
                                    </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-primary">ADD</button>
                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">RESET</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           </form> 
        </div>

 <link rel="stylesheet" href="{{asset('admin/assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css')}}">
<!-- css for data tables only -->
 <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>All Faculty Name</h2>
                            <ul class="header-dropdown dropdown">
                                
                                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another Action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                           <th>Serial No.</th>
                                           <th>Name</th>
                                           <th>Action</th>
                                       
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                           <th>Serial No.</th>
                                           <th>Name</th>
                                           <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($subjects as $subject)
                                        <tr>
                                           <td>{{$no++}}</td>
                                           <td>{{$subject->name}}</td>
                                           <td>
                                            <form id="delete-form-{{ $subject->id }}" action="{{ route('subject.destroy',[$subject->id]) }}" style="display: none;" method="POST">
                                               {{csrf_field()}}company
                                               {{ method_field('DELETE') }}
                                           </form>

                                      <button type="button" class="btn btn-danger" title="Delete" onclick="if(confirm('Are you sure? You want to delete this?')){
                                           event.preventDefault();
                                           document.getElementById('delete-form-{{ $subject->id }}').submit();
                                       }else {
                                           event.preventDefault();
                                               }">Delete
                                      </button> 
                                          </td>
                                        </tr>
                                        @endforeach                                       
                                       </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    </div>

@endsection
