
@extends('admin-layout.master')
@section('content')

<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Message</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Send Your Message for all guardian</a></li>
                            <!-- <li class="breadcrumb-item active" aria-current="page">Add All Department Name</li> -->
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="javascript:void(0);" class="btn btn-sm btn-primary" title="">Dashboard</a>
                        <a href="#" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a>
                    </div>
                </div>
            </div>
    <form role="form" action="{{route('message.store')}}" method="POST" enctype="multipart/form-data">
     @csrf
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <div class="row">

                                      @if ($errors->any())
                                            <div class="alert alert-warning alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                @if ($errors->count() == 1)
                                                   {{$errors->first()}}
                                                @else
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </div>
                                    @endif


                                <div class="row col-lg-12">
                                    <div class="col-lg-3">
                                    <label for="basic-url">Write your Message:</label>
                                    </div>
                                    <div class="col-lg-7">
                                    <div class="form-group">                                   
                                        <textarea name="message" rows="5" cols="50" class="form-control" placeholder="Write your message here"></textarea>
                                    </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-primary">Send All Guardian</button>
                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">RESET</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           </form> 
        </div>

 <link rel="stylesheet" href="{{asset('admin/assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css')}}">
<!-- css for data tables only -->

    </div>

@endsection
