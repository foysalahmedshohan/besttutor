
@extends('admin-layout.master')
@section('content')

<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
<div id="main-content">
       

 <link rel="stylesheet" href="{{asset('admin/assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css')}}">
<!-- css for data tables only -->
 <div class="col-lg-12">
                    <div class="card">
                        <div class="header">

                         <div class="block-header">
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-12">
                                    <h1>Class</h1>
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Best Tutor</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Add All Class Name</li>
                                        </ol>
                                    </nav>
                                </div>            
                                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary" title="">Dashboard</a>
                                    <a href="#" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a>
                                </div>
                            </div>
                        </div>
                        
                           <!--  <ul class="header-dropdown dropdown">
                                
                                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another Action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                           <th>Serial No.</th>
                                           <th>Email</th>
                                           <th>Mobile</th>
                                           <th>Gender</th>
                                            <th>Medium</th>
                                           <th>Address</th>
                                            <th>Class</th>
                                           <th>selery</th>
                                            <th>Days</th>
                                           <th>Action</th>
                                       
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                           <th>Serial No.</th>
                                           <th>Email</th>
                                           <th>Mobile</th>
                                           <th>Gender</th>
                                            <th>Medium</th>
                                           <th>Address</th>
                                            <th>Class</th>
                                           <th>selery</th>
                                            <th>Days</th>
                                           <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($posts as $post)
                                        <tr>
                                           <td>{{$no++}}</td>
                                           <td>{{$post->email}}</td>
                                           <td>{{$post->mobile}}</td>
                                           <td>{{$post->sgender}}</td>
                                           <td>{{$post->medium}}</td>
                                           <td>{{$post->address}}</td>
                                           <td>{{$post->class}}</td>
                                           <td>{{$post->selery}}</td>
                                           <td>{{$post->days}}</td>
                                           <td>
                                            <form id="delete-form-{{ $post->id }}" action="{{ route('admin.postDelete',[$post->id]) }}" style="display: none;" method="POST">
                                               {{csrf_field()}}company
                                               {{ method_field('DELETE') }}
                                           </form>

                                      <button type="button" class="btn btn-danger" title="Delete" onclick="if(confirm('Are you sure? You want to delete this?')){
                                           event.preventDefault();
                                           document.getElementById('delete-form-{{ $post->id }}').submit();
                                       }else {
                                           event.preventDefault();
                                               }">Delete
                                      </button> 
                                          </td>
                                        </tr>
                                        @endforeach                                       
                                       </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    </div>

@endsection
