<!doctype html>   
<html lang="en">
<!-- Mirrored from project.artificial-soft.com/admin/v4/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Sep 2020 04:37:44 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<title>Best Tutor | Admin</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="artificial-soft.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{asset('admin/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/vendor/font-awesome/css/font-awesome.min.css')}}')}}')}}">
<link rel="stylesheet" href="{{asset('admin/assets/vendor/animate-css/vivify.min.css')}}')}}">

<!-- MAIN CSS -->
<link rel="stylesheet" href="{{asset('admin/assets/site.min.css')}}">

</head>
<body class="theme-cyan font-montserrat light_version">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>

<!-- Theme Setting -->
<div class="themesetting">
    <a href="javascript:void(0);" class="theme_btn"><i class="icon-magic-wand"></i></a>
    <div class="card theme_color">
        <div class="header">
            <h2>Theme Color</h2>
        </div>
        <ul class="choose-skin list-unstyled mb-0">
            <li data-theme="green"><div class="green"></div></li>
            <li data-theme="orange"><div class="orange"></div></li>
            <li data-theme="blush"><div class="blush"></div></li>
            <li data-theme="cyan" class="active"><div class="cyan"></div></li>
            <li data-theme="indigo"><div class="indigo"></div></li>
            <li data-theme="red"><div class="red"></div></li>
        </ul>
    </div>
    <div class="card font_setting">
        <div class="header">
            <h2>Font Settings</h2>
        </div>
        <div>
            <div class="fancy-radio mb-2">
                <label><input name="font" value="font-krub" type="radio"><span><i></i>Krub Google font</span></label>
            </div>
            <div class="fancy-radio mb-2">
                <label><input name="font" value="font-montserrat" type="radio" checked><span><i></i>Montserrat Google font</span></label>
            </div>
            <div class="fancy-radio">
                <label><input name="font" value="font-roboto" type="radio"><span><i></i>Robot Google font</span></label>
            </div>
        </div>
    </div>
    <div class="card setting_switch">
        <div class="header">
            <h2>Settings</h2>
        </div>
        <ul class="list-group">
            <li class="list-group-item">
                Light Version
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="lv-btn" checked="">
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                RTL Version
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="rtl-btn">
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                Horizontal Henu
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="hmenu-btn" >
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                Mini Sidebar
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="mini-sidebar-btn">
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
        </ul>
    </div>
    <div class="card">
        <div class="form-group">
            <label class="d-block">Traffic this Month <span class="float-right">77%</span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="d-block">Server Load <span class="float-right">50%</span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
            </div>
        </div>
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<div id="wrapper">

    <nav class="navbar top-navbar">
        <div class="container-fluid">

            <div class="navbar-left">
                <div class="navbar-btn">
                    <a href="{{route('admin.index')}}"><img src="{{asset('admin/assets/images/logo.png')}}" alt="Lisbon Soft Logo" class="img-fluid logo"></a>
                    <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                </div>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="icon-envelope"></i>
                            <span class="notification-dot bg-green">4</span>
                        </a>
                        <ul class="dropdown-menu right_chat email vivify fadeIn">
                            <li class="header green">You have 4 New eMail</li>
                                                        <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <div class="avtar-pic w35 bg-red"><span>FC</span></div>
                                        <div class="media-body">
                                            <span class="name">James Wert <small class="float-right text-muted">Just now</small></span>
                                            <span class="message">Update GitHub</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                                                        <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <div class="avtar-pic w35 bg-red"><span>FC</span></div>
                                        <div class="media-body">
                                            <span class="name">James Wert <small class="float-right text-muted">Just now</small></span>
                                            <span class="message">Update GitHub</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                                                        <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <div class="avtar-pic w35 bg-red"><span>FC</span></div>
                                        <div class="media-body">
                                            <span class="name">James Wert <small class="float-right text-muted">Just now</small></span>
                                            <span class="message">Update GitHub</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                                                        <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <div class="avtar-pic w35 bg-red"><span>FC</span></div>
                                        <div class="media-body">
                                            <span class="name">James Wert <small class="float-right text-muted">Just now</small></span>
                                            <span class="message">Update GitHub</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                                                    </ul>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="icon-bell"></i>
                            <span class="notification-dot bg-azura">4</span>
                        </a>
                        <ul class="dropdown-menu feeds_widget vivify fadeIn">
                            <li class="header blue">You have 4 New Notifications</li>
                                                        <li>
                                <a href="javascript:void(0);">
                                    <div class="feeds-left bg-red"><i class="fa fa-check"></i></div>
                                    <div class="feeds-body">
                                        <h4 class="title text-danger">Issue Fixed <small class="float-right text-muted">9:10 AM</small></h4>
                                        <small>WE have fix all Design bug with Responsive</small>
                                    </div>
                                </a>
                            </li>
                                                        <li>
                                <a href="javascript:void(0);">
                                    <div class="feeds-left bg-red"><i class="fa fa-check"></i></div>
                                    <div class="feeds-body">
                                        <h4 class="title text-danger">Issue Fixed <small class="float-right text-muted">9:10 AM</small></h4>
                                        <small>WE have fix all Design bug with Responsive</small>
                                    </div>
                                </a>
                            </li>
                                                        <li>
                                <a href="javascript:void(0);">
                                    <div class="feeds-left bg-red"><i class="fa fa-check"></i></div>
                                    <div class="feeds-body">
                                        <h4 class="title text-danger">Issue Fixed <small class="float-right text-muted">9:10 AM</small></h4>
                                        <small>WE have fix all Design bug with Responsive</small>
                                    </div>
                                </a>
                            </li>
                                                    </ul>
                    </li>
                    <li class="dropdown language-menu">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="fa fa-language"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item pt-2 pb-2" href="#"><img src="{{asset('admin/assets/images/flag/us.svg')}}" class="w20 mr-2 rounded-circle"> English</a>
                            <a class="dropdown-item pt-2 pb-2" href="#"><img src="{{asset('admin/assets/images/flag/gb.svg')}}" class="w20 mr-2 rounded-circle"> Bengali</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item pt-2 pb-2" href="#"><img src="{{asset('admin/assets/images/flag/russia.svg')}}" class="w20 mr-2 rounded-circle"> Russian</a>
                            <a class="dropdown-item pt-2 pb-2" href="#"><img src="{{asset('admin/assets/images/flag/arabia.svg')}}" class="w20 mr-2 rounded-circle"> Arabic</a>
                            <a class="dropdown-item pt-2 pb-2" href="#"><img src="{{asset('admin/assets/images/flag/france.svg')}}" class="w20 mr-2 rounded-circle"> French</a>
                        </div>
                    </li>                    
                </ul>
            </div>
            
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li><a href="javascript:void(0);" class="search_toggle icon-menu" title="Search Result"><i class="icon-magnifier"></i></a></li>
            <li> <a class="" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">Logout                            
                 </a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                     @csrf
                    </form>
             </li>
                        <li><a href="login.html" class="icon-menu"><i class="icon-power"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="progress-container"><div class="progress-bar" id="myBar"></div></div>
    </nav>

    <div class="search_div">
        <div class="card">
            <div class="body">
                <form id="navbar-search" class="navbar-form search-form">
                    <div class="input-group mb-0">
                        <input type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="icon-magnifier"></i></span>
                            <a href="javascript:void(0);" class="search_toggle btn btn-danger"><i class="icon-close"></i></a>
                        </div>
                    </div>
                </form>
            </div>            
        </div>
        <span>Search Result <small class="float-right text-muted">About 90 results (0.47 seconds)</small></span>
        <div class="table-responsive">
            <table class="table table-hover table-custom spacing5">
                <tbody>
                                        <tr>
                        <td class="w40">
                            <span>01</span>
                        </td>   
                        <td>
                            <div class="d-flex align-items-center">
                                <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name"><span>SS</span></div>
                                <div class="ml-3">
                                    <a href="page-invoices-detail.html" title="">South Shyanne</a>
                                    <p class="mb-0">south.shyanne@example.com</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                                        <tr>
                        <td class="w40">
                            <span>01</span>
                        </td>   
                        <td>
                            <div class="d-flex align-items-center">
                                <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name"><span>SS</span></div>
                                <div class="ml-3">
                                    <a href="page-invoices-detail.html" title="">South Shyanne</a>
                                    <p class="mb-0">south.shyanne@example.com</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                                        <tr>
                        <td class="w40">
                            <span>01</span>
                        </td>   
                        <td>
                            <div class="d-flex align-items-center">
                                <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name"><span>SS</span></div>
                                <div class="ml-3">
                                    <a href="page-invoices-detail.html" title="">South Shyanne</a>
                                    <p class="mb-0">south.shyanne@example.com</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                                        <tr>
                        <td class="w40">
                            <span>01</span>
                        </td>   
                        <td>
                            <div class="d-flex align-items-center">
                                <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name"><span>SS</span></div>
                                <div class="ml-3">
                                    <a href="page-invoices-detail.html" title="">South Shyanne</a>
                                    <p class="mb-0">south.shyanne@example.com</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                                        <tr>
                        <td class="w40">
                            <span>01</span>
                        </td>   
                        <td>
                            <div class="d-flex align-items-center">
                                <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name"><span>SS</span></div>
                                <div class="ml-3">
                                    <a href="page-invoices-detail.html" title="">South Shyanne</a>
                                    <p class="mb-0">south.shyanne@example.com</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                                    </tbody>
            </table>
        </div>
    </div>

    <div id="left-sidebar" class="sidebar">
        <div class="navbar-brand">
            <a href="index.html"><img src="assets/images/logo.png" alt="Lisbon Soft Logo" class="img-fluid logo"></a>
            <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
        </div>
        <div class="sidebar-scroll">
            <div class="user-account">
                <div class="user_div">
                    <img src="{{asset('admin/assets/images/user.png')}}" class="user-photo" alt="User Profile Picture">
                </div>
                <div class="dropdown">
                    <span>Welcome</span>
                   <!--  <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>Louis Pierce</strong></a>
                    <ul class="dropdown-menu dropdown-menu-right account vivify flipInY">
                        <li><a href="#"><i class="icon-user"></i>My Profile</a></li>
                        <li><a href="#"><i class="icon-envelope-open"></i>Messages</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="icon-power"></i>Logout</a></li>
                    </ul> -->
                </div>                
            </div>  
            <nav id="left-sidebar-nav" class="sidebar-nav">
                <ul id="main-menu" class="metismenu">
                    <li class="header">Main</li>
                    <li class="active"><a href="{{route('admin.index')}}"><i class="icon-speedometer"></i><span>Dashboard</span></a></li>
                    <li><a href="{{route('faculty.index')}}" ><i class="icon-diamond"></i><span>Faculty</span></a></li>
                    <li><a href="{{route('department.index')}}"><i class="icon-diamond"></i><span>Department</span></a></li>
                    <!-- <li><a href=" "><i class="icon-diamond"></i><span>Area</span></a></li> -->
                    <li><a href="{{route('class.index')}}"><i class="icon-diamond"></i><span>Class</span></a></li>
                    <li><a href="{{route('subject.index')}}"><i class="icon-diamond"></i><span>Subject</span></a></li>
                   <!-- end --> 

                    <li class="header">Post</li>
                      <li><a href="{{route('admin.postList')}}"><i class="icon-diamond"></i><span>Post</span></a></li>
                      <li><a href="{{route('admin.tutorList')}}"><i class="icon-diamond"></i><span>Tutor</span></a></li>

                    <li class="header">Message</li>
                      <li><a href="{{route('message.index')}}"><i class="icon-diamond"></i><span>All Number</span></a></li>
                      <li><a href="{{route('admin.tutorList')}}"><i class="icon-diamond"></i><span>Individual Number</span></a></li>  
                   <!--  <li>
                        <a href="table.html" class="has-arrow"><i class="icon-book-open"></i><span>Table</span></a>
                        <ul>
                                                        <li><a href="table.html"><i class="icon-book-open"></i> Normal Table</a></li>
                            <li><a href="data-table.html"><i class="icon-book-open"></i> Data Table</a></li>
                                                        <li><a href="table.html"><i class="icon-book-open"></i> Normal Table</a></li>
                            <li><a href="data-table.html"><i class="icon-book-open"></i> Data Table</a></li>
                                                    </ul>
                    </li>
                    <li>
                        <a href="table.html" class="has-arrow"><i class="icon-lock"></i><span>Login</span></a>
                        <ul>
                            <li><a href="login.html"><i class="icon-book-open"></i> Login 1</a></li>
                            <li><a href="login-wide.html"><i class="icon-book-open"></i> Login 2</a></li>
                            <li><a href="forgot-password.html"><i class="icon-book-open"></i> Forgot Password</a></li>
                            <li><a href="404.html"><i class="icon-book-open"></i> 404 Error</a></li>
                        </ul>
                    </li>
                    <li><a href="widgets.html"><i class="icon-bubbles"></i><span>Others Features</span></a></li>
                                        <li class="header">App</li>
                    <li>
                        <a href="table.html" class="has-arrow"><i class="icon-book-open"></i><span>Table</span></a>
                        <ul>
                                                        <li><a href="table.html"><i class="icon-book-open"></i> Normal Table</a></li>
                            <li><a href="data-table.html"><i class="icon-book-open"></i> Data Table</a></li>
                                                        <li><a href="table.html"><i class="icon-book-open"></i> Normal Table</a></li>
                            <li><a href="data-table.html"><i class="icon-book-open"></i> Data Table</a></li>
                                                    </ul>
                    </li>
                    <li>
                        <a href="table.html" class="has-arrow"><i class="icon-lock"></i><span>Login</span></a>
                        <ul>
                            <li><a href="login.html"><i class="icon-book-open"></i> Login 1</a></li>
                            <li><a href="login-wide.html"><i class="icon-book-open"></i> Login 2</a></li>
                            <li><a href="forgot-password.html"><i class="icon-book-open"></i> Forgot Password</a></li>
                            <li><a href="404.html"><i class="icon-book-open"></i> 404 Error</a></li>
                        </ul>
                    <li>
                    <li><a href="widgets.html"><i class="icon-bubbles"></i><span>Others Features</span></a></li>
                                        <li class="header">App</li>
                    <li>
                        <a href="table.html" class="has-arrow"><i class="icon-book-open"></i><span>Table</span></a>
                        <ul>
                                                        <li><a href="table.html"><i class="icon-book-open"></i> Normal Table</a></li>
                            <li><a href="data-table.html"><i class="icon-book-open"></i> Data Table</a></li>
                                                        <li><a href="table.html"><i class="icon-book-open"></i> Normal Table</a></li>
                            <li><a href="data-table.html"><i class="icon-book-open"></i> Data Table</a></li>
                                                    </ul>
                    </li>
                    <li>
                        <a href="table.html" class="has-arrow"><i class="icon-lock"></i><span>Login</span></a>
                        <ul>
                            <li><a href="login.html"><i class="icon-book-open"></i> Login 1</a></li>
                            <li><a href="login-wide.html"><i class="icon-book-open"></i> Login 2</a></li>
                            <li><a href="forgot-password.html"><i class="icon-book-open"></i> Forgot Password</a></li>
                            <li><a href="404.html"><i class="icon-book-open"></i> 404 Error</a></li>
                        </ul>
                    </li>
                    <li><a href="widgets.html"><i class="icon-bubbles"></i><span>Others Features</span></a></li>
                                    </ul> -->
                                </ul>
            </nav>     
        </div>
    </div>
  @yield('content')

 </div>
     <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
     <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
     <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  
  


<!-- Javascript -->
<script src="{{asset('admin/assets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{asset('admin/assets/bundles/vendorscripts.bundle.js')}}"></script>
<script src="{{asset('admin/assets/bundles/flotscripts.bundle.js')}}"></script>

<script src="{{asset('admin/assets/bundles/mainscripts.bundle.js')}}"></script>
<script src="{{asset('admin/assets/bundles/hrdashboard.js')}}"></script>

<!-- data table -->

<!-- script for data tables only -->
<script src="{{asset('admin/assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{asset('admin/assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('admin/assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('admin/assets/vendor/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('admin/assets/vendor/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
<script src="{{asset('admin/assets/bundles/mainscripts.bundle.js')}}"></script>
<script src="{{asset('admin/assets/bundles/jquery-datatable.js')}}"></script>
</body>

<!-- Mirrored from project.artificial-soft.com/admin/v4/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Sep 2020 04:38:06 GMT -->
</html>
