@extends('tutorProfileLayout.tutor_master')
@section('tutor_content')

@if($value==0)
<div class="adpost-details container">
   <form role="form" action="{{route('tutorProfile.store')}}" method="POST" enctype="multipart/form-data">
     @csrf
   <div class="section postdetails">

   <div class="section seller-info">
  <div class="col-lg-12">
      @if ($errors->any())
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        @if ($errors->count() == 1)
                           {{$errors->first()}}
                        @else
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
            @endif
   @if(session()->has('message'))
              <div class="alert alert-success">
                  {{ session()->get('message') }}
              </div>
          @endif
   

  <div class="row">
      <div class="col-lg-6">
          <div class="row form-group">
          <label class="col-sm-4 label-title">Name<span class="required">*</span></label>
          <div class="col-sm-8">
          <input type="text" name="name" class="form-control" placeholder="ex, Dhaka Model College">
          </div>
          </div>
           <div class="row form-group">                               
              <label class="col-sm-4 label-title">University Name<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="university form-control select2" id="university" name="university" value="" style="width: 100%;">     
                  @foreach($universitys as $university)                          
                    <option value="{{$university->name}}">{{$university->name}}</option>
                    @endforeach                                  
                    </select>
               </div> 
               </div>

          <div class="row form-group">
          <label class="col-sm-4 label-title">SSC Academic Institutin<span class="required">*</span></label>
          <div class="col-sm-8">
          <input type="text" name="ssc_ins" class="form-control" placeholder="ex, Dhaka Model College">
          </div>
          </div>
       
           
            <div class="row form-group">                               
              <label class="col-sm-4 label-title">Faculty<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="faculty form-control js-example-basic-single" id="faculty" name="faculty" value="" style="width: 100%;">
                    @foreach ($facultys as $faculty)                               
                    <option value="{{$faculty->name}}">{{$faculty->name}}</option>   @endforeach                              
                    </select>
               </div> 
               </div>                         
                          
              <div class="row form-group">                               
              <label class="col-sm-4 label-title">Department<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="department form-control js-example-basic-single" id="department" name="department" value="" style="width: 100%;">
                     @foreach ($departments as $department)                               
                    <option value="{{$department->name}}">{{$department->name}}</option>   @endforeach                             
                    </select>
               </div> 
               </div>

               <div class="row form-group">                               
              <label class="col-sm-4 label-title">Education Medium Background<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="emb form-control js-example-basic-single" id="emb" name="emb" value="" style="width: 100%;">
                    <!-- <option>Select</option>                                      -->
                    <option value="English">English</option>   
                    <option value="Bangla">Bangla</option>  
                    <option value="Arabic">Arabic</option>  
                    <option value="Urdu<">Urdu</option>                               
                    </select>
               </div> 
               </div>

              <div class="row form-group">                               
              <label class="col-sm-4 label-title">Country<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class=" country form-control select2 country" id="country" name="country" value="" style="width: 100%;" >                       
                  @foreach($countrys as $country)                               
                   <option value="{{$country->id}}">{{$country->name}}</option> 
                  @endforeach 
                    </select>
               </div> 
               </div> 

               <div class="row form-group">                               
              <label class="col-sm-4 label-title">Division<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="division form-control select2 division" id="division" name="division" value="" style="width: 100%;"disabled>
                <option></option>
                	@foreach($divisions as $division)                               
                   <option value="{{$division->id}}">{{$division->name}}</option> 
                  @endforeach                                 
                 </select>
               </div> 
               </div> 

              <div class="row form-group">                               
              <label class="col-sm-4 label-title">District<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="district form-control select2" id="district" name="district" value="" style="width: 100%;"disabled>
                 <option></option>
                    @foreach ($districs as $distric)                               
                    <option value="{{$distric->id}}">{{$distric->name}}</option>   @endforeach                                 
                    </select>
               </div> 
               </div>

              <div class="row form-group">                               
              <label class="col-sm-4 label-title">City<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="city form-control select2" id="city" name="city" value="" style="width: 100%;"disabled>
                 <option></option>
                    @foreach ($upazilas as $city)                               
                    <option value="{{$city->id}}">{{$city->name}}</option>   @endforeach                                  
                    </select>
               </div> 
               </div>
               <div class="row form-group">                               
              <label class="col-sm-4 label-title">Area<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="area form-control select2" id="area" name="area" value="" style="width: 100%;"disabled>
                 <option></option>
                    @foreach ($unions as $union)                               
                    <option value="{{$union->id}}">{{$union->name}}</option>   @endforeach                                    
                    </select>
               </div> 
               </div>
                             
           
            <!-- <div class="checkbox section agreement"> -->
            <!-- </div> -->
            </div>
          

        <div class="col-lg-6">
        	  

               <div class="row form-group">
            <label class="col-sm-4 label-title">HSC Academic Institutin<span class="required">*</span></label>
            <div class="col-sm-8">
            <input type="text" name="hsc_ins" class="form-control " placeholder="Required Subject">
            </div>
            </div>  

            <div class="row form-group">                               
              <label class="col-sm-4 label-title">Preferred Class<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="pre_class form-control select2" id="pre_class" name="pre_class" value="" style="width: 100%;">
                    @foreach ($preClasses as $preClass)                               
                    <option value="{{$preClass->name}}">{{$preClass->name}}</option>   @endforeach                                 
                    </select>
               </div> 
             </div>
              <div class="row form-group">                               
              <label class="col-sm-4 label-title">Preferred Subjects<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="pre_subject form-control select2" id="pre_subject" name="pre_subject" value="" style="width: 100%;">
                    @foreach ($subjects as $subject)                               
                    <option value="{{$subject->name}}">{{$subject->name}}</option>   @endforeach                                  
                    </select>
               </div> 
             </div>
             <div class="row form-group">
                <label class="col-sm-4 label-title">Date of Birth<span class="required">*</span></label>
                <div class="col-sm-8">
                <input type="date" name="bdate" class="form-control" placeholder="Required Subject">
                </div>
                </div> 


                <div class="row form-group">
                <label class="col-sm-4 label-title">Address<span class="required">*</span></label>
                <div class="col-sm-8">
                <input type="text" name="address" class="form-control" placeholder="Required Subject">
                </div>
                </div> 
                
             
              <div class="row form-group">
                <label class="col-sm-4 label-title">Gender<span class="required">*</span></label>
                <div class="col-sm-8">
                <input type="radio" name="gender" value="Male" id="individual">
                <label for="individual">Male</label>
                <input type="radio" name="gender" value="Female" id="dealer">
                <label for="dealer">Female</label>
                </div>
              </div> 
               <div class="row form-group">
                <label class="col-sm-4 label-title">NID No.<span class="required">*</span></label>
                <div class="col-sm-8">
                <input type="text" name="nid" class="form-control" placeholder="Required Subject">
                </div>
                </div>

                 <div class="row form-group">
                <label class="col-sm-4 label-title">Expected Selery<span class="required">*</span></label>
                <div class="col-sm-8">
                <input type="number" name="selery" class="form-control" placeholder="Required Subject">
                </div>
                </div>

                <div class="row form-group">
                <label class="col-sm-4 label-title">Father Number<span class="required">*</span></label>
                <div class="col-sm-8">
                <input type="text" name="f_mobile" class="form-control" placeholder="Required Subject">
                </div>
                </div>

                <div class="row form-group">
                <label class="col-sm-4 label-title">Mother Number<span class="required">*</span></label>
                <div class="col-sm-8">
                <input type="text" name="m_mobile" class="form-control" placeholder="Required Subject">
                </div>
                 <input type="hidden" name="user_id" class="form-control" value="{{Auth::user()->id}}">
                </div> 
             
          <br><br>
          </div>
          <button type="submit" class="btn btn-primary" value="submit">{{ __('Save') }}</button>
          </form>
          </div>
          
      @elseif($value==1)
      <br>
      <br>
      <div class="container">
      <div class=" section slider">
      <h3> {{$profileinfo->name}} </h3>
      <div class="row">
      <div class="col-lg-4">
      <div class="slider-text">
      <span class="icon"><i class="fa fa-clock-o"></i><a href="#">Joining Data: {{ $profileinfo->bdate}} </a></span>
      <div class="short-info">
          <p><strong>Faculty : </strong><a href="#">FSIT</a> </p>
          <p><strong>Department : </strong><a href="#">CSE </a> </p>
          <p><strong>Education M. B. : </strong><a href="#">{{ $profileinfo->emb }} </a> </p>
          <p><strong>Exp. Selery : </strong><a href="#">{{ $profileinfo->selery }}</a></p>
          <p><strong>Country: </strong><a href="#">Bangladesh</a></p>
          <p><strong>Division: </strong><a href="#">Dhaka</a></p>
          <p><strong>Father Mobile : </strong><a href="#">{{ $profileinfo->f_mobile }}</a></p>

         
      </div>
      <div class="contact-with">
      <!-- <h4>Contact with </h4> -->
      <a href="#" class="btn"><i class="fa"></i>Delete Acoount</a>
     <!--  <a href="#" class="btn"><i class="fa"></i>Direction</a>
      <a href="#" class="btn"><i class="fa"></i>Apply</a> -->
      </div>

      </div>
      </div>

      <div class="col-lg-4">
      <div class="slider-text">

      <br>
      <div class="short-info">
      <p><strong>Mother Mobile : </strong><a href="#">{{ $profileinfo->m_mobile }}</a></p>
     <p><strong>SSC Ins. Name :</strong><a href="#">{{ $profileinfo->ssc_ins }}</a> </p>
          <p><strong>HSC Ins. Name :</strong><a href="#">{{ $profileinfo->hsc_ins }}</a> </p>
          <p><strong>Class :</strong><a href="#">{{ $profileinfo->pre_class }}</a> </p>
          <p><strong>Preffered Subject : </strong><a href="#">{{ $profileinfo->pre_subject }}</a></p>
          <p><strong>Address : </strong><a href="#">{{ $profileinfo->address }}</a></p>
               <p><strong>Country: </strong><a href="#">{{ $country }}</a></p> 
     <p><strong>Division: </strong><a href="#">{{ $division }}</a></p>
         

      </div>
      </div>
      </div>

       <div class="col-lg-4">
      <div class="slider-text">

      <br>
      <div class="short-info">

     <p><strong>District: </strong><a href="#">{{ $district }}</a></p>
          <p><strong>City: </strong><a href="#">{{ $upazila }}</a></p>
          <p><strong>Area: </strong><a href="#">{{ $union }}</a></p>
          <p><strong>Teacher Gender : </strong><a href="#">{{ $profileinfo->gender }}</a> </p>
          <p><strong>Gender : </strong><a href="#">{{ $profileinfo->gender }}</a></p>
          <p><strong>NID: </strong><a href="#">{{ $profileinfo->nid }}</a></p>
          

      </div>
      </div>
      </div>


      </div>
      </div>
      </div>
       @endif

     </div>
    </div>
  </div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
        $('#country').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('/ajax/division')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('division').removeAttribute("disabled");
                            $("#division").empty();
                            $("#division").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#division").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#division").empty();
                        }
                    }
                });
        });

  </script>

  <script type="text/javascript">
        $('#division').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('/ajax/district')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('district').removeAttribute("disabled");
                            $("#district").empty();
                            $("#district").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#district").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#district").empty();
                        }
                    }
                });
        });

  </script>

  <script type="text/javascript">
        $('#district').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('/ajax/upazila')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('city').removeAttribute("disabled");
                            $("#city").empty();
                            $("#city").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#city").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#city").empty();
                        }
                    }
                });
        });

  </script>

    <script type="text/javascript">
        $('#city').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('/ajax/area')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('area').removeAttribute("disabled");
                            $("#area").empty();
                            $("#area").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#area").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#area").empty();
                        }
                    }
                });
        });

  </script>



@endsection