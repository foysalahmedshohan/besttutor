@extends('layout.master')

@section('content')

<br>

<div class="container">
<div class="section trending-ads cars-ads">
<div class="section-title">
<h4>265 Jobs Found</h4>
</div>
@foreach($posts as $post)
<div class="ad-item row">

<div class="item-image-box  col col-lg-4">
<div class="item-image">
<br>
<a href="details.html"><img src="{{asset('assets/images/user.jpg')}}" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">
</div>

<h4 class="item-title"><a href="#">{{$post->subject}} Teacher</a></h4>
<h3 class="item-price">{{$post->selery}}</h3>
</div>
</div>
<div class="cars-ads-box colcol-lg-8">

<div class="colcol-lg-5 car-info">
<ul>
<li><i class="fa fa-calendar" aria-hidden="true"></i>Tutoring Days: {{$post->days}} </li>
<li><i class="fa fa-tachometer" aria-hidden="true"></i>Student Gneder: {{$post->sgender}} </li>
<li><i class="fa fa-external-link" aria-hidden="true"></i>Starting Date: {{$post->jDate}} </li>
<li><i class="fa fa-tachometer" aria-hidden="true"></i>Teacher Gender: {{$post->tgender}} </li>
<!-- <li><i class="fa fa-external-link" aria-hidden="true"></i>Tutoring Time: {{$post->class}} </li> -->
</ul>
</div>
<div class="colcol-lg-7 car-info">
<ul>
<!-- <li><i class="fa fa-eyedropper" aria-hidden="true"></i>Catagory: {{$post->category}} </li> -->
<li><i class="fa fa-bitbucket" aria-hidden="true"></i>No of Student:  {{$post->noStudent}} </li>
<li><i class="fa fa-database" aria-hidden="true"></i>Subject: {{$post->subject}} </li>

<li><i class="fa fa-database" aria-hidden="true"></i>Country:{{$post->country}} </li>
</ul>
</div>
<div class="ad-meta"><br>
<div class="meta-content">
<span class="dated"><a href="#">Catagory: {{$post->category}}  </a></span>
<a href="#" class="tag"><i class="fa fa-map-marker"></i> Location: {{$post->address}} |</a>
<a href="#" class="tag"><i class="fa fa-database"></i>  Instition Name: {{$post->Daffodil}}</a>

</div>
</div>
<a href="{{route('post.postView',[$post->id])}}"><button type="button" class="btn btn-primary" style="margin-left: 25px;margin-top: 62px">More Details</button></a>


</div>
</div>
@endforeach
</div>
</div>


<div class="text-center">
{{ $posts->links() }}
</div>




@endsection