@extends('tutorProfileLayout.tutor_master')
@section('tutor_content')


<section id="" class="clearfix  ad-profile-page">
<div class="container">

<div class="profile">
<div class="row">


<div class="col-md-12">
<div class="user-pro-section">

<div class="profile-details section">
<h2>Profile Details</h2>


<div class="form-group">
<label>Email:</label>
<input type="text" class="form-control" disabled placeholder="{{ Auth::user()->email }}">
</div>
<div class="form-group">
<label>Phone</label>
<input type="email" class="form-control" disabled  placeholder="{{ Auth::user()->phone }}">
</div>
 <div class="form-group">
<label for="name-three">Reference Phone</label>
<input type="text" class="form-control" disabled placeholder="{{ Auth::user()->r_phone }}">
</div>
<!-- <div class="form-group">
<label>Your City</label>
<select class="form-control">
<option value="#">Los Angeles, USA</option>
<option value="#">Dhaka, BD</option>
<option value="#">Shanghai</option>
<option value="#">Karachi</option>
<option value="#">Beijing</option>
<option value="#">Lagos</option>
<option value="#">Delhi</option>
<option value="#">Tianjin</option>
<option value="#">Rio de Janeiro</option>
</select>
</div> -->
<a href="#" class="btn">Complete Profile</a>
</div>

<!-- <div class="change-password section">
<h2>Change password</h2>

<div class="form-group">
<label>Old Password</label>
<input type="password" class="form-control">
</div>
<div class="form-group">
<label>New password</label>
<input type="password" class="form-control">
</div>
<div class="form-group">
<label>Confirm password</label>
<input type="password" class="form-control">
</div>
</div> -->

<!-- <div class="preferences-settings section">
<h2>Preferences Settings</h2>

<div class="checkbox">
<label><input type="checkbox" name="logged"> Comments are enabled on my ads </label>
<label><input type="checkbox" name="receive"> I want to receive newsletter.</label>
<label><input type="checkbox" name="want">I want to receive advice on buying and selling. </label>
</div>
</div> -->
<!-- <a href="#" class="btn">Update Profile</a>
<a href="#" class="btn cancle">Cancle</a> -->
</div>
</div>

</div>
</div>
</div>
</section>




@endsection