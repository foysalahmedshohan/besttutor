@extends('layout.master')

@section('content')

<section id="main" class="clearfix user-page">
<div class="container">
<div class="row text-center">

<div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
<div class="user-account">
<h2>User Login</h2>

<form action="#">
<div class="form-group">
<!-- <select class="form-control">
<option value="#">Tutor</option>
<option value="#">Student/Guardian</option>
</select> -->
<input type="text" class="form-control" placeholder="Username">
</div>
<div class="form-group">
<input type="password" class="form-control" placeholder="Password">
</div>
<button type="submit" class="btn">Login</button>
</form>

<div class="user-option">
<div class="checkbox pull-left">
<label for="logged"><input type="checkbox" name="logged" id="logged"> Keep me logged in </label>
</div>
<div class="pull-right forgot-password">
<a href="#">Forgot password</a>
</div>
</div>
</div>
<a href="{{route('register')}}" class="btn-primary">Create a New Account</a>

</div>
</div>
</div>
</section>



@endsection