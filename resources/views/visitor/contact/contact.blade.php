@extends('layout.master')

@section('content')
<section id="main" class="clearfix contact-us">
<div class="container">

<ol class="breadcrumb">
<li><a href="index-2.html">Home</a></li>
<li>Contact</li>
 </ol>
<h2 class="title">Contact Us</h2>

<!-- <div id="gmap"></div> -->
<div class="corporate-info">
<div class="row">

<div class="col-md-4">
<div class="contact-info">
<h2>Corporate Info</h2>
<address>
<p><strong>adress: </strong>1234 Street Name, City Name, Country</p>
<p><strong>Phone:</strong> <a href="#">(123) 456-7890</a></p>
<p><strong>Email: </strong><a href="#"><span class="__cf_email__" data-cfemail="ee87808881ae8d81839e8f8097c08d8183">[email&#160;protected]</span></a></p>
</address>
<ul class="social">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
</ul>
</div>
</div>

<div class="col-md-8">
<div class="feedback">
<h2>Send Us Your Feedback</h2>
<form id="contact-form" class="contact-form" name="contact-form" method="post" action="#">
<div class="row">
<div class="col-md-6">
<div class="form-group">
<input type="text" class="form-control" required="required" placeholder="Name">
</div>
</div>
<div class="col-md-6">
<div class="form-group">
<input type="email" class="form-control" required="required" placeholder="Email Id">
</div>
</div>
<div class="col-md-12">
<div class="form-group">
<input type="text" class="form-control" required="required" placeholder="Subject">
</div>
</div>
<div class="col-sm-12">
<div class="form-group">
<textarea name="message" id="message" required="required" class="form-control" rows="7" placeholder="Message"></textarea>
</div>
</div>
</div>
<div class="form-group">
<button type="submit" class="btn">Submit Your Message</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</section>


@endsection