@extends('layout.master')

@section('content')

<br>

<div class="container">

<div class="section trending-ads cars-ads">
<div class="section-title">
<h4>Trending Ads</h4>
</div>
<div class="ad-item row">
<div class="item-image-box  col col-lg-5">
<div class="item-image">
<a href="details.html"><img src="images/car/5.jpg" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">
<span><a href="#">Hyundai</a></span>
</div>
<h4 class="item-title"><a href="#">2015 Hyundai Santa Fe</a></h4>
<h3 class="item-price">$80,000.00</h3>
</div>
</div>
<div class="cars-ads-box colcol-lg-7">
<div class="car-info">
<ul>
<li><i class="fa fa-calendar" aria-hidden="true"></i>Registration date: 2015</li>
<li><i class="fa fa-tachometer" aria-hidden="true"></i>Mileage: 6,000 mi</li>
<li><i class="fa fa-external-link" aria-hidden="true"></i>Condition: Used</li>
</ul>
</div>
<div class="car-info">
<ul>
<li><i class="fa fa-eyedropper" aria-hidden="true"></i>Exterior Color: Silver</li>
<li><i class="fa fa-bitbucket" aria-hidden="true"></i>Interior Color: Brown (Leather)</li>
<li><i class="fa fa-database" aria-hidden="true"></i>Drivetrain: front-engine, front- or 4-wheel-drive</li>
</ul>
</div>
<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan, 16 10:10 pm </a></span>
<a href="#" class="tag"><i class="fa fa-tags"></i> Used</a>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dealer"><i class="fa fa-suitcase"></i> </a>
</div>
</div>
</div>
</div>
<div class="ad-item row">
<div class="item-image-box  col col-lg-5">
<div class="item-image">
<a href="details.html"><img src="images/car/6.jpg" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">
<span><a href="#">Ford</a></span>
</div>
<h4 class="item-title"><a href="#">2017 Ford Focus</a></h4>
<h3 class="item-price">$25,000.00 <span>(Negotiable)</span></h3>
</div>
</div>
<div class="cars-ads-box col-lg-7">
<div class="car-info">
<ul>
<li><i class="fa fa-calendar" aria-hidden="true"></i>Registration date: 2015</li>
<li><i class="fa fa-tachometer" aria-hidden="true"></i>Mileage: 6,000 mi</li>
<li><i class="fa fa-external-link" aria-hidden="true"></i>Condition: Used</li>
</ul>
</div>
<div class="car-info">
<ul>
<li><i class="fa fa-eyedropper" aria-hidden="true"></i>Exterior Color: Silver</li>
<li><i class="fa fa-bitbucket" aria-hidden="true"></i>Interior Color: Brown (Leather)</li>
<li><i class="fa fa-database" aria-hidden="true"></i>Drivetrain: front-engine, front- or 4-wheel-drive</li>
</ul>
</div>
<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan, 16 10:10 pm </a></span>
<a href="#" class="tag"><i class="fa fa-tags"></i> Used</a>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dealer"><i class="fa fa-suitcase"></i> </a>
</div>
</div>
</div>
</div>
<div class="ad-item row">
<div class="item-image-box col col-lg-5">
<div class="item-image">
<a href="details.html"><img src="images/car/7.jpg" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">
<span><a href="#">Mercedes Benz</a></span>
</div>
<h4 class="item-title"><a href="#">2018 Mercedes-Benz Sprinter</a></h4>
<h3 class="item-price">$89,000.00</h3>
</div>
</div>
<div class="cars-ads-box col-lg-7">
<div class="car-info">
<ul>
<li><i class="fa fa-calendar" aria-hidden="true"></i>Registration date: 2015</li>
<li><i class="fa fa-tachometer" aria-hidden="true"></i>Mileage: 6,000 mi</li>
<li><i class="fa fa-external-link" aria-hidden="true"></i>Condition: Used</li>
</ul>
</div>
<div class="car-info">
<ul>
<li><i class="fa fa-eyedropper" aria-hidden="true"></i>Exterior Color: Silver</li>
<li><i class="fa fa-bitbucket" aria-hidden="true"></i>Interior Color: Brown (Leather)</li>
<li><i class="fa fa-database" aria-hidden="true"></i>Drivetrain: front-engine, front- or 4-wheel-drive</li>
</ul>
</div>
<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan, 16 10:10 pm </a></span>
<a href="#" class="tag"><i class="fa fa-tags"></i> Used</a>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dealer"><i class="fa fa-suitcase"></i> </a>
</div>
</div>
</div>
</div>
<div class="ad-item row">
<div class="item-image-box  col col-lg-5">
<div class="item-image">
<a href="details.html"><img src="images/car/8.jpg" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">
<span><a href="#">Toyota</a></span>
</div>
<h4 class="item-title"><a href="#">2018 Toyota RAV4</a></h4>
<h3 class="item-price">$12,800.00</h3>
</div>
</div>
<div class="cars-ads-box col-lg-7">
<div class="car-info">
<ul>
<li><i class="fa fa-calendar" aria-hidden="true"></i>Registration date: 2015</li>
<li><i class="fa fa-tachometer" aria-hidden="true"></i>Mileage: 6,000 mi</li>
<li><i class="fa fa-external-link" aria-hidden="true"></i>Condition: Used</li>
</ul>
</div>
<div class="car-info">
<ul>
<li><i class="fa fa-eyedropper" aria-hidden="true"></i>Exterior Color: Silver</li>
<li><i class="fa fa-bitbucket" aria-hidden="true"></i>Interior Color: Brown (Leather)</li>
<li><i class="fa fa-database" aria-hidden="true"></i>Drivetrain: front-engine, front- or 4-wheel-drive</li>
</ul>
</div>
<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan, 16 10:10 pm </a></span>
<a href="#" class="tag"><i class="fa fa-tags"></i> Used</a>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dealer"><i class="fa fa-suitcase"></i> </a>
</div>
</div>
</div>
</div>
</div>

</div>
@endsection