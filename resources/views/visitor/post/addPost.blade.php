@extends('layout.master')

@section('content')

<section id="" class="clearfix ad-details-page" style="margin-top: -30px">
<div class="container">
<!-- profile part -->
<section id="main" class="clearfix  ad-profile-page">
<div class="container">
<div class="ad-profile section">
<div class="user-profile">
<div class="user-images">
<img src="{{asset('assets/images/user.jpg')}}" alt="User Images" class="img-fluid">
</div>
<div class="user">
<h2>Hello, <a href="#">Dear</a></h2>
<h5>You Can Publish Your Job Post Here</h5>
</div>
<div class="favorites-user">
<div class="my-ads">
<a href="my-ads.html">23<small>My ADS</small></a>
</div>
<!-- <div class="favorites">
<a href="#">18<small>Favorites</small></a>
</div> -->
</div>
</div>
</div>
</div>
</section>
<!-- profile part end -->




<div class="adpost-details" style="margin-top: -30px">
<div class="row col-lg-12">


<div class="row">

<div class="col-lg-6">
 <form role="form" action="{{route('store')}}" method="POST" enctype="multipart/form-data">
     @csrf

</div>

<!-- second part -->
<div class="col-lg-6">

</div>
</div>
</div>

</div>
</section>


<div class="adpost-details container">
   
   <div class="section postdetails">

   <div class="section seller-info">
  <div class="col-lg-12">
        @if ($errors->any())
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        @if ($errors->count() == 1)
                           {{$errors->first()}}
                        @else
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
            @endif
   @if(session()->has('message'))
              <div class="alert alert-success">
                  {{ session()->get('message') }}
              </div>
          @endif
  <div class="row">

  <!-- <form role="form" action="{{route('store')}}" method="POST" enctype="multipart/form-data"> -->
     <!-- @csrf -->
  <!-- <h3>  Need Bangla Medium Tutor For HSC- 1st Year Student - 3 Days / Week</h3> -->
      <div class="col-lg-6">


          <div class="row form-group">                               
              <label class="col-sm-4 label-title">Preffered Teacher Institute Name<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="tinsName form-control select2" id="tinsName" name="tinsName" value="" style="width: 100%;">
                 <option>Select</option>
                    @foreach ($universitys as $university)                               
                    <option value="{{$university->name}}">{{$university->name}}</option>   @endforeach                                    
                    </select>
               </div> 
            </div>


           <div class="row form-group">
            <label class="col-sm-4 label-title">Email<span class="required">*</span></label>
            <div class="col-sm-8">
            <input type="email" name="email" class="form-control" placeholder="Required Subject">
            </div>
            </div>                      
                          
              <div class="row form-group">                               
              <label class="col-sm-4 label-title">Tuition Type<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="tutionType form-control select2" id="tutionType" name="tutionType" value="" style="width: 100%;">
                    <!-- <option>Select</option>                                      -->
                    <option value="Home Tutoring">Home Tutoring</option>   
                    <option value="Online Tutoring">Online Tutoring</option>  
                    <option value="Group Tutoring">Group Tutoring</option>  
                    <option value="Package Tutoring">Package Tutoring</option>                               
                    </select>
               </div> 
               </div>

            <div class="row form-group">
             <label class="col-sm-4">Student Gender<span class="required">*</span></label>
             <div class="col-sm-8 user-type">
             <input type="radio" name="sgender" value="Male" id="newsell"> <label for="newsell">Male </label>
             <input type="radio" name="sgender" value="Female" id="newbuy"> <label for="newbuy">Female</label>
              <input type="radio" name="sgender" value="Both" id="newbuy2"> <label for="newbuy">Both</label>
             </div>
             </div>


            <div class="row form-group select-condition">
            <label class="col-sm-4">Medium<span class="required">*</span></label>
            <div class="col-sm-8">
            <input type="radio" name="medium" value="English" id="new">
            <label for="new">English</label>

            <input type="radio" name="medium" value="Bangla" id="used">
            <label for="used">Bangla</label>

            <input type="radio" name="medium" value="Arabic" id="OLD">
            <label for="old">Arabic</label>
            </div>
            </div>


               <div class="row form-group">                               
              <label class="col-sm-4 label-title">Subject<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="subject form-control select2" id="subject" name="subject" value="" style="width: 100%;">
                 <option>Select</option>
                    @foreach ($subjects as $subject)                               
                    <option value="{{$subject->name}}">{{$subject->name}}</option>   @endforeach                                    
                    </select>
               </div> 
               </div>

                <div class="row form-group">                               
              <label class="col-sm-4 label-title">Department<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="department form-control select2" id="department" name="department" value="" style="width: 100%;">
                 <option>Select</option>
                    @foreach ($departments as $department)                               
                    <option value="{{$department->name}}">{{$department->name}}</option>   @endforeach                                    
                    </select>
               </div> 
               </div> 


               <div class="row form-group">                               
              <label class="col-sm-4 label-title">Country<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class=" country form-control select2 country" id="country" name="country" value="" style="width: 100%;" >                       
                  @foreach($countrys as $country)                               
                   <option value="{{$country->id}}">{{$country->name}}</option> 
                  @endforeach 
                    </select>
               </div> 
               </div> 

                <div class="row form-group">                               
              <label class="col-sm-4 label-title">Division<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="division form-control select2 division" id="division" name="division" value="" style="width: 100%;"disabled>
                <option></option>
                  @foreach($divisions as $division)                               
                   <option value="{{$division->id}}">{{$division->name}}</option> 
                  @endforeach                                 
                 </select>
               </div> 
               </div> 

               <div class="row form-group">                               
              <label class="col-sm-4 label-title">District<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="district form-control select2" id="district" name="district" value="" style="width: 100%;"disabled>
                 <option></option>
                    @foreach ($districs as $distric)                               
                    <option value="{{$distric->id}}">{{$distric->name}}</option>   @endforeach                                 
                    </select>
               </div> 
               </div>

               <div class="row form-group">                               
              <label class="col-sm-4 label-title">City<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="city form-control select2" id="city" name="city" value="" style="width: 100%;"disabled>
                 <option></option>
                    @foreach ($upazilas as $city)                               
                    <option value="{{$city->id}}">{{$city->name}}</option>   @endforeach                                  
                    </select>
               </div> 
               </div>
              <div class="row form-group">                               
              <label class="col-sm-4 label-title">Area<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="area form-control select2" id="area" name="area" value="" style="width: 100%;"disabled>
                 <option></option>
                    @foreach ($unions as $union)                               
                    <option value="{{$union->id}}">{{$union->name}}</option>   @endforeach                                    
                    </select>
               </div> 
               </div>  


                 <div class="row form-group">                               
              <label class="col-sm-4 label-title">Category<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="form-control select2" id="category" name="category" value="" style="width: 100%;">
                    <option>Select</option>                                     
                    <option value="Religious Studies">Religious Studies</option>  
                    <option value="Admission Test">Admission Test</option>                               
                    <option value="Arts">Arts</option>                                  
                    <option value="Language Learning">Language Learning</option>                                  
                    <option value="Test Preparation">Test Preparation</option>                                  
                    <option value="Professional Skill">Professional Skill</option>                                  
                    <option value="Special Skills">Special Skills</option>                                  
                    <option value="Project/Assignment">Project/Assignment</option>                                  
                    <option value="Local Guide">Local Guide</option>                                 
                    </select>
               </div> 
               </div>  
              

            <div class="row form-group">                               
              <label class="col-sm-4 label-title">Class<span class="required">*</span></label>
              <div class="col-sm-8">
              <select class="class form-control select2" id="class" name="class" value="" style="width: 100%;">
                 <option>Select</option>
                    @foreach ($preClasses as $class)                               
                    <option value="{{$class->name}}">{{$class->name}}</option>   @endforeach                                    
                    </select>
               </div> 
               </div>

                                    
           
            <!-- <div class="checkbox section agreement"> -->
            
            <!-- </div> -->

            </div>
          

        <div class="col-lg-6">
            <div class="row form-group">
                <label class="col-sm-4 label-title">Student Institute Name<span class="required">*</span></label>
                <div class="col-sm-8">
                <input type="text" name="sins_name" id="sins_name" class="form-control" placeholder="Required Subject">
                </div>
               </div> 

           <div class="row form-group">
                <label class="col-sm-4 label-title">Teacher Gender Preferance<span class="required">*</span></label>
                <div class="col-sm-8">
                <input type="radio" name="tgender" value="Male" id="individual">
                <label for="individual">Male</label>
                <input type="radio" name="tgender" value="Female" id="dealer">
                <label for="dealer">Female</label>
                <input type="radio" name="tgender" value="Any" id="dealer">
                <label for="dealer">Any</label>
                </div>
              </div>

            

            <div class="row form-group add-title">
            <label class="col-sm-4 label-title">Address<span class="required">*</span></label>
            <div class="col-sm-8">
            <input type="text" class="form-control" id="address" name="address" placeholder="Enter your Add">
            </div>
            </div>


            <div class="row form-group">                               
            <label class="col-sm-4 label-title">No. of Students<span class="required">*</span></label>
            <div class="col-sm-8">
         <select class="form-control select2" id="noStudent" name="noStudent" value="" style="width: 100%;">
                  <option value="1">1</option>                                     
                  <option value="2">2</option>                                  
                  <option value="3">3</option>
                  <option value="4">4</option>                                     
                  <option value="5">5</option>                                  
                  <option value="6">6</option>
                  <option value="7">7</option>                                  
                  <option value="8">8</option>   
                  <option value="9">9</option>
                  <option value="10">10</option>                                                                       
               </select>
             </div> 
             </div> 
             <div class="row form-group">                               
            <label class="col-sm-4 label-title">Days / Week<span class="required">*</span></label>
            <div class="col-sm-8">
            <select class="form-control select2" id="days" name="days" value="" style="width: 100%;">
                  <option value="1">1</option>                                     
                  <option value="2">2</option>                                  
                  <option value="3">3</option>
                  <option value="4">4</option>                                     
                  <option value="5">5</option>                                  
                  <option value="6">6</option>
                  <option value="7">7</option>                                      
                  </select>
             </div> 
             </div> 

             <div class="row form-group">
          <label class="col-sm-4 label-title">Joining Date<span class="required">*</span></label>
          <div class="col-sm-8">
          <input type="date" name="jDate" id="jDate" class="form-control" placeholder="dd/mm/yyyy">
          </div>
          </div>

          <!--  <div class="row form-group">
          <label class="col-sm-3 label-title">Time<span class="required"></span></label>
          <div class="col-sm-9">
          <input type="time" name="time" id="time" class="form-control" placeholder="dd/mm/yyyy">
          </div>
          </div> -->

          <div class="row form-group">
          <label class="col-sm-4 label-title">Mobile<span class="required"></span>*</label>
          <div class="col-sm-8">
          <input type="mobile" name="mobile" class="form-control" placeholder="ex, 016852***26,">
          </div>
          </div>
          <div class="row form-group">
          <label class="col-sm-4 label-title">Alternative Mobile<span class="required"></span>*</label>
          <div class="col-sm-8">
          <input type="a_mobile" name="a_mobile" class="form-control" placeholder="ex, 016852***26,">
          </div>
          </div>

          <div class="row form-group">
          <label class="col-sm-4 label-title">Selery</label>
          <div class="col-sm-8">
          <input type="number" name="selery" class="form-control" placeholder="ex, Selery">
          </div>
          </div>

            <div class="row form-group">                               
            <label class="col-sm-4 label-title">How did you hear about us?<span class="required">*</span></label>
            <div class="col-sm-8">
            <select class="form-control select2" id="hereAboutUs" name="hereAboutUs" value="" style="width: 100%;">                                    
                  <option value="Friends">Friends</option> 
                  <option value="Family">Family</option> 
                  <option value="Television">Television</option>                                  
                  </select>
             </div> 
             </div> 

          <div class="row form-group item-description">
          <label class="col-sm-4 label-title">Special Requirment<span class="required">*</span></label>
          <div class="col-sm-8">
          <textarea class="form-control" id="s_requirment" name="s_requirment" placeholder="Write few lines about your products" rows="8"></textarea>
          </div>
          </div>  
          <br><br>
          </div>
          <button type="submit" class="btn btn-primary" value="submit">{{ __('Save') }}</button>
          </form>
          </div>

     </div>
    </div>
  </div>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
        $('#country').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('/ajax/division')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('division').removeAttribute("disabled");
                            $("#division").empty();
                            $("#division").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#division").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#division").empty();
                        }
                    }
                });
        });

  </script>

  <script type="text/javascript">
        $('#division').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('/ajax/district')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('district').removeAttribute("disabled");
                            $("#district").empty();
                            $("#district").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#district").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#district").empty();
                        }
                    }
                });
        });

  </script>

  <script type="text/javascript">
        $('#district').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('/ajax/upazila')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('city').removeAttribute("disabled");
                            $("#city").empty();
                            $("#city").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#city").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#city").empty();
                        }
                    }
                });
        });

  </script>

    <script type="text/javascript">
        $('#city').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('/ajax/area')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('area').removeAttribute("disabled");
                            $("#area").empty();
                            $("#area").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#area").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#area").empty();
                        }
                    }
                });
        });

  </script>

@endsection
