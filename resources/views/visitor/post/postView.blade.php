



@extends('layout.master')

@section('content')
 
 <br>
<div class="container">
<div class=" section slider">
<div class="row">

<h3>  Need {{ $post->medium }} Medium Tutor For {{ $post->class }} Student - 
	{{ $post->days }} Days / Week</h3>

<div class="col-lg-7">
<div class="slider-text">
<p><span>Job ID :<a href="#"> {{ $post->created_at }} </a></span><br>
<span class="icon"><i class="fa fa-clock-o"></i><a href="#"> {{ $post->jDate }}</a></span>
<span class="icon"><i class="fa fa-map-marker"></i><a href="#">{{ $post->address }}</a></span>

<div class="short-info">
<p><strong>Selery: </strong><a href="#">{{ $post->selery }}</a></p>
<p><strong>Teacher Gender : </strong><a href="#">{{ $post->tgender }}</a> </p>
<p><strong>Student Gender : </strong><a href="#">{{ $post->sgender }} </a> </p>
<p><strong>Institution : </strong><a href="#">{{ $post->sins_name }} </a> </p>
<p><strong>Address : </strong><a href="#">{{ $post->address }} </a> </p>
<p><strong>Medium : </strong><a href="#">{{ $post->medium }} </a> </p>
<p><strong>Preffered Tutor Institute Name : </strong><a href="#">{{ $post->tinsname }} </a> </p>


</div>
<div class="contact-with">
<h4>Contact with </h4>
<a href="#" class="btn"><i class="fa"></i>Location</a>
<a href="#" class="btn"><i class="fa"></i>Direction</a>
<a href="#" class="btn"><i class="fa"></i>Apply</a>
</div>

</div>
</div>

<div class="col-lg-5">
<div class="slider-text">

<br><br><br>
<div class="short-info">
<p><strong>No. of Student :</strong><a href="#">{{ $post->noStudent }}</a> </p>
<p><strong>Class :</strong><a href="#">{{ $post->class}} </a> </p>
<p><strong>Medium : </strong><a href="#">{{ $post->medium }} </a> </p>
<p><strong>Subjects :</strong><a href="#">{{ $post->subject }}</a> </p>
<p><strong>Category :</strong><a href="#">{{ $post->category }}</a> </p>
<p><strong>Extra Requirment : </strong><a href="#">{{ $post->s_requirment }}</a></p>
<p><strong>Mobile : </strong><a href="#">{{ $post->mobile }}</a></p>

</div>
</div>
</div>
</div>
</div>
</div>

@endsection