


@extends('layout.master')
@section('content')
<section id="main" class="clearfix user-page">
<div class="container">
<div class="row text-center">

<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2">
<div class="user-account">
<h2>You Can Register Here</h2>
<form method="POST" action="{{ route('register') }}">
@csrf
<!-- <select class="form-control">
<option value="#">Tutor</option>
<option value="#">Student/Guardian</option>
</select> -->
<div class="form-group row">
                            <label for="aboutus" class="col-md-4 col-form-label text-md-right">{{ __('Where you had heared about us?') }}</label>
                            <div class="col-md-6">
                               <select class="form-control" id="aboutus" name="aboutus">
                                <option value="Family">Family</option>
                                <option value="Friends">Friends</option>
                                <option value="Online">Online</option>
                                <option value="Newspaper">Newspaper</option>
                                </select>
                            </div>
                        </div>
 <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
  <div class="form-group row">
                        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Mobile') }}</label>
                        <div class="col-md-6">
                        <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" required >
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        </div>
                        </div>
 <div class="form-group row">
                        <label for="r_phone" class="col-md-4 col-form-label text-md-right">{{ __('Reference Mobile') }}</label>
                        <div class="col-md-6">
                        <input id="r_phone" type="text" class="form-control @error('r_phone') is-invalid @enderror" name="r_phone" required >
                        @error('r_phone')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        </div>
                        </div>
 <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
 <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                                         

<div class="checkbox">
<label class="pull-left checked" for="signing"><input type="checkbox" name="signing" id="signing"> By signing up for an account you agree to our Terms and Conditions </label>
</div>
<button type="submit" class="btn">Registration</button>
<a href="{{route('login')}}"><button type="button" class="btn">Log In</button></a>
</form>

</div>
</div>
</div>
</div>
</section>



@endsection







