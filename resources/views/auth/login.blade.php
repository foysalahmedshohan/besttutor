<!--  <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
 -->

@extends('layout.master')
@section('content')
<section id="main" class="clearfix user-page">
<div class="container">
<div class="row text-center">

<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2">
<div class="user-account">
<h2>User Login</h2>

 <form method="POST" action="{{ route('login') }}">
  @csrf
  <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
     <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
    <button type="submit" class="btn">Login</button>
</form>
<div class="user-option">
    <div class="checkbox pull-left">
    <label for="logged"><input type="checkbox" name="logged" id="logged"> Keep me logged in </label>
    </div>
    <div class="pull-right forgot-password">
    @if (Route::has('password.request'))
    <a href="{{ route('password.request') }}">Forgot password</a>
     @endif
    </div>
</div>
</div>
<a href="{{route('register')}}" class="btn-primary">Create a New Account</a>
</div>
</div>
</div>
</section>
@endsection