@extends('layout.master')

@section('content')


<section class="home-banner">
<div class="banner-overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="car-info">
<ul class="socail list-inline">
<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
<li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
<li><a href="#" title="Youtube"><i class="fa fa-youtube"></i></a></li>
</ul>
<h1> Hire The Right Tutor Today!</h1>
<h3>Book one-on-one lessons with verified tutors in your area</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
<a href="{{route('register')}}" class="btn btn-primary">Hire a tutor (It's Free!) </a>
</div>
</div>

<!-- <div class="col-md-4 offset-md-2">
<div class="contact-form">
<h1>Find Your Tutor</h1>
<div class="contact-info">
<form action="#">
<h3>Tutor Type:</h3>
<div class="dropdown category-dropdown">
<a data-toggle="dropdown" href="#" aria-expanded="false"><span class="change-text">All</span> <i class="fa fa-angle-down"></i></a>
<ul class="dropdown-menu category-change">
<li><a href="#">Option 1</a></li>
<li><a href="#">Option 2</a></li>
<li><a href="#">Option 3</a></li>
</ul>
</div>
<h3>Select Makes</h3>
<div class="dropdown category-dropdown">
<a data-toggle="dropdown" href="#" aria-expanded="false"><span class="change-text">All</span> <i class="fa fa-angle-down"></i></a>
<ul class="dropdown-menu category-change">
<li><a href="#">Option 1</a></li>
<li><a href="#">Option 2</a></li>
<li><a href="#">Option 3</a></li>
</ul>
</div>
 <h3>Budget:</h3>
<div class="dropdown category-dropdown">
<a data-toggle="dropdown" href="#" aria-expanded="false"><span class="change-text">All</span> <i class="fa fa-angle-down"></i></a>
<ul class="dropdown-menu category-change">
<li><a href="#">Option 1</a></li>
<li><a href="#">Option 2</a></li>
<li><a href="#">Option 3</a></li>
</ul>
</div>
<button type="submit" class="form-control" value="Search">Search</button>
</form>
</div>
</div>
</div> -->
</div>
</div>
</section>
<br>





<div class="container">
<div class="  section cta text-center" id="count_section">
<div class="row">

<div class="col-lg-3">
<div class="single-cta">

<div class="cta-icon icon-secure">
<img src="{{asset('assets/images/icon/7.png')}}" alt="Icon" class="img-fluid">
</div>
<h4>1526</h4>
<p>Total Applied</p>
</div>
</div>

<div class="col-lg-3">
<div class="single-cta">

<div class="cta-icon icon-support">
<img src="{{asset('assets/images/icon/8.png')}}   " alt="Icon" class="img-fluid">
</div>
<h4>1041</h4>
<p>Live Tuition Jobs</p>
</div>
</div>

<div class="col-lg-3">
<div class="single-cta">

<div class="cta-icon icon-trading">
<img src="{{asset('assets/images/icon/5.png')}} " alt="Icon" class="img-fluid">
</div>
<h4>33052</h4>
<p>Happy Students</p>
</div>
</div>
<div class="col-lg-3">
<div class="single-cta">

<div class="cta-icon icon-trading">
<img src="{{asset('assets/images/icon/10.png')}} " alt="Icon" class="img-fluid">
</div>
<h4>4.6/5</h4>
<p>Average Tutor Rating</p>
</div>
</div>
</div>
</div>

</div>


<!-- district section -->


<div class="page">
<div class="container">

<div class="section featureds">
<div class="section-title featured-top">
<h4>Live Tuition Jobs</h4>
</div>
<div class="featured-slider">
<div id="featured-slider-two">
<div class="featured">
<div class="featured-image">
<a href="details.html"><img src="{{asset('assets/images/car/1.jpg')}}" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">

</div>
<h4 class="item-title"><a href="#">Barisal Division</a></h4>
<h3 class="item-price">25865</h3>
</div>

<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan 10:10 pm </a></span>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a href="#" data-toggle="tooltip" data-placement="top" title="Individual"><i class="fa fa-suitcase"></i> </a>
</div>
</div> 
</div>
<div class="featured">
<div class="featured-image">
<a href="details.html"><img src="{{asset('assets/images/car/2.jpg')}}" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">

</div>
<h4 class="item-title"><a href="#">Chittagong Division</a></h4>
<h3 class="item-price">582585</h3>
</div>

<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan 10:10 pm </a></span>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a href="#" data-toggle="tooltip" data-placement="top" title="Individual"><i class="fa fa-user"></i> </a>
</div>
</div>
</div>
<div class="featured">
<div class="featured-image">
<a href="details.html"><img src="{{asset('assets/images/car/3.jpg')}}" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">

</div>
<h4 class="item-title"><a href="#">Dhaka Division</a></h4>
<h3 class="item-price">5286</h3>
</div>

<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan 10:10 pm </a></span>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a href="#" data-toggle="tooltip" data-placement="top" title="Individual"><i class="fa fa-suitcase"></i> </a>
</div>
</div>
</div>
<div class="featured">
<div class="featured-image">
<a href="details.html"><img src="{{asset('assets/images/car/4.jpg')}}" alt="Image" class="img-fluid"></a>
<a href="#" class="verified" data-toggle="tooltip" data-placement="left" title="Verified"><i class=""></i></a>
</div>
<div class="ad-info">
<div class="item-cat">

</div>
<h4 class="item-title"><a href="#">Khulna Division</a></h4>
<h3 class="item-price">5824154</h3>
</div>

<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan 10:10 pm </a></span>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a href="#" data-toggle="tooltip" data-placement="top" title="Individual"><i class="fa fa-user"></i> </a>
</div>
</div>
</div>
<div class="featured">
<div class="featured-image">
<a href="details.html"><img src="{{asset('assets/images/car/5.jpg')}}" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">

</div>
<h4 class="item-title"><a href="#">Mymensingh Division</a></h4>
<h3 class="item-price">540014</h3>
</div>

<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan 10:10 pm </a></span>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a href="#" data-toggle="tooltip" data-placement="top" title="Individual"><i class="fa fa-suitcase"></i> </a>
</div>
</div>
</div>
<div class="featured">
<div class="featured-image">
<a href="details.html"><img src="{{asset('assets/images/car/6.jpg')}}" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">

</div>
<h4 class="item-title"><a href="#">Rajshahi Division</a></h4>
<h3 class="item-price">50525</h3>
</div>

<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan 10:10 pm </a></span>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a href="#" data-toggle="tooltip" data-placement="top" title="Individual"><i class="fa fa-user"></i> </a>
</div>
</div>
</div>
<div class="featured">
<div class="featured-image">
<a href="details.html"><img src="{{asset('assets/images/car/7.jpg')}}" alt="Image" class="img-fluid"></a>
</div>
<div class="ad-info">
<div class="item-cat">

</div>
<h4 class="item-title"><a href="#">Rangpur Division</a></h4>
<h3 class="item-price">51415</h3>
</div>

<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan 10:10 pm </a></span>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a href="#" data-toggle="tooltip" data-placement="top" title="Individual"><i class="fa fa-suitcase"></i> </a>
</div>
</div>
</div>
<div class="featured">
<div class="featured-image">
<a href="details.html"><img src="{{asset('assets/images/car/4.jpg')}}" alt="Image" class="img-fluid"></a>
<a href="#" class="verified" data-toggle="tooltip" data-placement="left" title="Verified"><i class=""></i></a>
</div>
<div class="ad-info">
<div class="item-cat">

</div>
<h4 class="item-title"><a href="#">Sylhet Division</a></h4>
<h3 class="item-price">5145414</h3>
</div>

<div class="ad-meta">
<div class="meta-content">
<span class="dated"><a href="#">7 Jan 10:10 pm </a></span>
</div>

<div class="user-option pull-right">
<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
<a href="#" data-toggle="tooltip" data-placement="top" title="Individual"><i class="fa fa-user"></i> </a>
</div>
</div>
</div>
</div>
</div>
</div>


<!-- How it works for students/parents section -->
<div class="cta text-center">
<h4>How it works for students/parents?</h4>
<hr>
<br>
<div class="row">

<div class="col-md-4">
<div class="single-cta">

<div class="cta-icon icon-secure">
<img src="{{asset('assets/images/icon/post.svg')}}" alt="Icon" class="img-fluid"><br>
</div>
<h4>Post Your
Tutor Requirements</h4>
<p>Post your Tutor requirements. Our experts will analyze it and live your requirements to our job board.</p>
</div>
</div>

<div class="col-md-4">
<div class="single-cta">

<div class="cta-icon icon-support">
<img src=" {{asset('assets/images/icon/get.svg')}} " alt="Icon" class="img-fluid"><br><br>
</div>
<h4>Get the Maximum
5 Best Tutor CVs</h4>
<p>You'll receive the 5 (max) best Tutors' CVs in your account within 48 hours which closely match to your requirements.</p>
</div>
</div>

<div class="col-md-4">
<div class="single-cta">

<div class="cta-icon icon-trading">
<img src=" {{asset('assets/images/icon/select-learnigng.svg')}} " alt="Icon" class="img-fluid"><br>
</div>
<h4>Select the Best One
& Start Learning</h4>
<p>Choose the best one among the 5 CV's. Offer the selected Tutor for few trial classes before taking the regular classes.</p>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- parents comment -->
<div class="container">
<div class=" section slider">
<div class="row">

<div class="col-lg-7">
<div id="product-carousel" class="carousel slide" data-ride="carousel">

<!-- <ol class="carousel-indicators">
<li data-target="#product-carousel" data-slide-to="0" class="active">
<img src="{{asset('assets/images/slider/list-1.jpg')}}" alt="Carousel Thumb" class="img-fluid">
</li>
<li data-target="#product-carousel" data-slide-to="1">
<img src="{{asset('assets/images/slider/list-2.jpg')}}" alt="Carousel Thumb" class="img-fluid">
</li>
<li data-target="#product-carousel" data-slide-to="2">
<img src="{{asset('assets/images/slider/list-3.jpg')}}" alt="Carousel Thumb" class="img-fluid">
</li>
<li data-target="#product-carousel" data-slide-to="3">
<img src="{{asset('assets/images/slider/list-4.jpg')}}" alt="Carousel Thumb" class="img-fluid">
</li>
<li data-target="#product-carousel" data-slide-to="4">
<img src="{{asset('assets/images/slider/list-5.jpg')}}" alt="Carousel Thumb" class="img-fluid">
</li>
</ol> -->
<br>

<div class="carousel-inner" role="listbox">

<div class="item carousel-item active">
<div class="carousel-image">

<img src="{{asset('assets/images/icon/Zakaria_Habib.jpg')}}" alt="Featured Image" class="img-fluid">
</div>
</div>

<div class="item carousel-item">
<div class="carousel-image">

<img src="{{asset('assets/images/slider/2.jpg')}}" alt="Featured Image" class="img-fluid">
</div>
</div>


<div class="item carousel-item">
<div class="carousel-image">

<img src="{{asset('assets/images/slider/5.jpg')}}" alt="Featured Image" class="img-fluid">
</div>
</div>
</div>

<a class="left carousel-control" href="#product-carousel" role="button" data-slide="prev">
<i class="fa fa-chevron-left"></i>
</a>
<a class="right carousel-control" href="#product-carousel" role="button" data-slide="next">
<i class="fa fa-chevron-right"></i>
</a>
</div>
</div>

<div class="col-lg-5">
<div class="slider-text"><br>
<h2>What Some Awesome Parent Says About Us</h2>
<h3 class="title">Apple iPhone 6 16GB</h3>


<div class="short-info">
<h4>I am very satisfied with this platform. Caretutors.com really help me to find the right tutor. I will recommend this service to  others.</h4>
<p><strong>Foysal Ahmed </strong> </p>
<p><strong>Shashongacha, Cumilla </strong> </p>

</div>

<div class="contact-with">
<h4>Contact with </h4>
<span class="btn btn-red show-number">
<i class="fa fa-phone-square"></i>
<span class="hide-text">Hire A tutor </span>
<span class="hide-number">012-1234567890</span>
</span>
<!-- <a href="#" class="btn"><i class="fa fa-envelope-square"></i>Reply by email</a> -->
</div>

<!-- <div class="social-links">
<h4>Share this ad</h4>
<ul class="list-inline">
<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
<li><a href="#"><i class="fa fa-tumblr-square"></i></a></li>
</ul>
</div> -->
</div>
</div>
</div>
</div>
</div>
<!-- parents comment end-->

<div class="container">
<div class=" cta text-center">
<h4>Serving Categories</h4>
<hr>
<br>
<div class="row">

<div class="col-md-4">
<div class="single-cta">

<div class="cta-icon icon-secure">
<img src="{{asset('assets/images/icon/Test_Preparation.png')}}" alt="Icon" class="img-fluid">
</div>
<h4>Secure Trading</h4>
<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie</p>
</div>
</div>

<div class="col-md-4">
<div class="single-cta">

<div class="cta-icon icon-support"><br><br>
<img src=" {{asset('assets/images/icon/Professional_Skill.png')}} " alt="Icon" class="img-fluid">
</div><br>
<h4>24/7 Support</h4>
<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit</p>
</div>
</div>

<div class="col-md-4">
<div class="single-cta">

<div class="cta-icon icon-trading">
<img src=" {{asset('assets/images/icon/Project_Assignment.png')}} " alt="Icon" class="img-fluid">
</div>
<h4>Easy Trading</h4>
<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram</p>
</div>
</div>
</div>
</div>
</div>
<br><br>

<!-- how it works for tutor -->
<div class="container">
<div class=" text-center">
	<!-- <div class="cta text-center"> -->
<h4>How it works for tutors?</h4>
<hr>
<br>
<div class="row">

<div class="col-md-4">
<div class="single-cta">

<div class="cta-icon icon-secure">
<img src="{{asset('assets/images/icon/user.svg')}}" alt="Icon" class="img-fluid">
</div><br>
<h4>Create a Free
Account</h4>
<p>Make your profile in minutes. Add your educational information, preferred locations, classes/courses, and make your profile presentable.</p>
</div>
</div>

<div class="col-md-4">
<div class="single-cta">

<div class="cta-icon icon-support">
<img src=" {{asset('assets/images/icon/applyjob.svg')}} " alt="Icon" class="img-fluid">
</div><br>
<h4>Apply to Your Desired
Tuition Job</h4>
<p>Check our job board everyday and apply to your preferred tutoring jobs that match your skills.</p>
</div>
</div>

<div class="col-md-4">
<div class="single-cta">

<div class="cta-icon icon-trading">
<img src=" {{asset('assets/images/icon/maleTutoring.svg')}} " alt="Icon" class="img-fluid">
</div><br>
<h4>Start Tutoring</h4>
<p>Get selected by students/parents if your expertise matches their requirements.</p>
</div>
</div>
</div>
</div>
</div>

<!-- //// -->




<section id="download" class="clearfix parallax-section car-app-store">
<div class="container">
<div class="row">
<div class="col-sm-12 text-center">
<h2>Download on App Store</h2>
</div>
</div>

<div class="row">

<div class="col-md-4">
<a href="#" class="download-app">
<img src="{{asset('assets/images/icon/16.png')}}" alt="Image" class="img-fluid">
<span class="pull-left">
<span>available on</span>
<strong>Google Play</strong>
</span>
</a>
</div>

<div class="col-md-4">
<a href="#" class="download-app">
<img src="{{asset('assets/images/icon/17.png')}}" alt="Image" class="img-fluid">
<span class="pull-left">
<span>available on</span>
<strong>App Store</strong>
</span>
</a>
</div>

<div class="col-md-4">
<a href="#" class="download-app">
<img src="{{asset('assets/images/icon/18.png')}}" alt="Image" class="img-fluid">
<span class="pull-left">
<span>available on</span>
<strong>Windows Store</strong>
</span>
</a>
</div>
</div>
</div>
</section>




@endsection