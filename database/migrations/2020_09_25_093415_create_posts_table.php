<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
        
            $table->bigIncrements('id');
             $table->string('email')->nullable();
             $table->string('tinsName')->nullable();
             $table->string('tutionType')->nullable();
             $table->string('sgender')->comment('male=1, female=2, any=3')->nullable();
             $table->string('medium')->nullable();
             $table->string('subject')->nullable();
             $table->string('department')->nullable();
             $table->integer('country')->nullable();
             $table->integer('division')->nullable();
             $table->integer('district')->nullable();
             $table->integer('city')->nullable();
             $table->integer('area')->nullable();
             $table->string('category')->nullable();
             $table->string('class')->nullable();
             $table->string('sins_name')->nullable();
             $table->string('tgender')->nullable();
             $table->string('address')->nullable();
             $table->integer('noStudent')->nullable();
             $table->integer('days')->nullable();
             $table->string('jDate')->nullable();
             $table->string('mobile')->nullable();
             $table->string('a_mobile')->nullable();
             $table->integer('selery')->nullable();
             $table->string('hereAboutUs')->nullable();
             $table->string('s_requirment')->nullable();
             $table->timestamps();

    // $table->string('tutionType')->default('1')->comment('home=1, online=2, group=3, package=4');
    //         $table->integer('division')->nullable();
    //         $table->integer('district')->nullable();
    //         $table->string('category')->nullable();
    //         $table->string('class')->nullable();
    //         $table->string('subject')->nullable();
    //         $table->integer('gender')->default('1')->comment('male=1, female=2')->nullable();
    // $table->integer('genderPrefered')->default('1')->comment('male=1, female=2, any=3')->nullable();
    //         $table->string('address');
    //         $table->integer('noStudent');
    //         $table->integer('days');
    //         $table->string('jDate');
    //         $table->string('time')->nullable();
    //         $table->string('mobile');
    //         $table->string('selery');
    //         $table->string('hereAboutUs')->nullable();
    //         $table->string('description')->nullable();
            


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
