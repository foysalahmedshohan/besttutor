<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTprofilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tprofiles', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('name');
            $table->string('university')->nullable();
            $table->string('ssc_ins');
            $table->string('hsc_ins');            
            $table->string('faculty');
            $table->string('department');
            $table->string('emb');
            $table->string('country')->nullable();
            $table->string('division')->nullable();
            $table->string('district')->nullable();
            $table->string('city')->nullable();
            $table->string('area')->nullable();
            $table->string('pre_class');
            $table->string('pre_subject');
            $table->string('bdate');
            $table->string('address');
            $table->string('gender');
            $table->string('nid');
            $table->integer('selery');
            $table->string('f_mobile');
            $table->string('m_mobile')->nullable();    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tprofiles');
    }
}
