<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/admin-index', function () { 
//     return view('admin/index');
// });


Route::get('/admin-Login', function () { 
    return view('admin/login/login');
});



Route::get('/signup', function () {
    return view('visitor/signup');
});



Route::get('/contact', function () {
    return view('visitor/contact/contact');
});


// Route::group(['prefix' => 'manufacturers'], function() {
    Route::get('/', 'OthersSectionController@index')->name('dashboard');
    Route::get('/signin', 'OthersSectionController@signin')->name('signin');
    Route::get('/register', 'OthersSectionController@register')->name('register');
    Route::get('/contact', 'OthersSectionController@contact')->name('contact');
    Route::get('/admin-index', 'OthersSectionController@adminindex')->name('admin.index');


    Route::group(['prefix' => '/post'], function() {
    Route::resource('/', 'PostController');
  
   
});

    // Route::get('/{id}/edit', 'ManufacturerController@edit')->name('manufacturers.edit');
    // Route::post('/{id}/update', 'ManufacturerController@update')->name('manufacturers.update');
    // Route::get('/{id}/delete', 'ManufacturerController@delete')->name('manufacturers.delete');

// });

// Faculty
 Route::group(['prefix' => '/admin'], function() {
 Route::resource('/faculty', 'Admin\FacultyController');  
 });
 // department
 Route::group(['prefix' => '/admin'], function() {
 Route::resource('/department', 'Admin\DepartmentController');  
 });
 // class
 Route::group(['prefix' => '/admin'], function() {
 Route::resource('/class', 'Admin\ClassController');  
 });
  // subject
 Route::group(['prefix' => '/admin'], function() {
 Route::resource('/subject', 'Admin\SubjectController');  
 });
 //message
  Route::group(['prefix' => '/admin'], function() {
 Route::resource('/message', 'Admin\MessageController');  
 });
 // manage
 Route::get('/admin/postList', 'Admin\ManageController@postList')->name('admin.postList');
 Route::DELETE('/admin/postList/{id}/delete', 'Admin\ManageController@postDelete')->name('admin.postDelete');

 Route::get('/admin/tutorList', 'Admin\ManageController@tutorList')->name('admin.tutorList');
 Route::DELETE('/admin/tutorList/{id}/delete', 'Admin\ManageController@tutorDelete')->name('admin.tutorDelete');

Route::get('/ajax/division', 'Admin\AddressController@division');
Route::get('/ajax/district', 'Admin\AddressController@district');
Route::get('/ajax/upazila', 'Admin\AddressController@upazila');
Route::get('/ajax/area', 'Admin\AddressController@area');


 //Route::get('/admin-LoginPage', 'Auth\LoginController@login_page');

//admin login route






// Route::get('/sms','SmsController@sms');
Route::get('/code-verify','VerifyController@codeverify');
Route::post('/post-verify','VerifyController@postVerify')->name('post.verify');
Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

// Tutor profile
Auth::routes();
Route::middleware('auth')->group(function () {

Route::group(['prefix' => '/'], function() {
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/tutorProfile', 'TutorProfileController');
Route::get('/tutorProfileList', 'TutorProfileController@postList')->name('tutorProfile.postList'); 
Route::get('/postView/{id}', 'PostController@show')->name('post.postView');  
});

  Route::get('/admin-LoginPage', 'Auth\LoginController@login_page')->name('admin.adminLogin');
  Route::get('/admin-index', 'Auth\LoginController@admin_index')->name('admin.index');

});