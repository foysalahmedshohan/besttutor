<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('/guardianPost', 'API\PostController');
Route::post('/apiRegister', 'API\RegisterController@register');
Route::post('/apiLogin', 'API\LoginController@login');
Route::resource('/tutorProfile', 'API\TutorProfileController');


Route::get('/country', 'API\AddressController@country_show')->name('apiCountry.show');
Route::get('/division/{id}', 'API\AddressController@division_show')->name('apiDivision.show');
Route::get('/district/{id}', 'API\AddressController@district_show')->name('apiDistrict.show');
Route::get('/upazila/{id}', 'API\AddressController@upazila_show')->name('apiUpazila.show');
Route::get('/area/{id}', 'API\AddressController@area_show')->name('apiArea.show');


Route::get('/class', 'API\ClassController@class');
Route::get('/department', 'API\DepartmentController@department');
Route::get('/faculty', 'API\FacultyController@faculty');
Route::get('/subject', 'API\SubjectController@subject');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
