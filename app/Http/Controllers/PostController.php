<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Post;
use App\PostInst;
use App\Country;
use App\Division;
use App\District;
use App\Upazila;
use App\Union;
use App\Department;
use App\PreClass;
use App\Subject;
use App\Faculty;
use App\University;
use App\Tprofile;
use DB;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('visitor.post.postList');
        $count=Post::count();
        $posts=Post::paginate(5);
        return view('visitor.post.postList', compact('posts','count'))->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $countrys=Country::orderBy('name', 'asc')->get();
        $divisions=Division::orderBy('name', 'asc')->get();
        $districs=District::orderBy('name', 'asc')->get();
        $upazilas=Upazila::orderBy('name', 'asc')->get();
        $unions=Union::orderBy('name', 'asc')->get();
        $departments=Department::orderBy('name', 'asc')->get();
        $preClasses=PreClass::orderBy('name', 'asc')->get();
        $subjects=Subject::orderBy('name', 'asc')->get();
        $facultys=Faculty::orderBy('name', 'asc')->get();
        $universitys=University::orderBy('name', 'asc')->get();
        // return $university,$facultys;
        return view('visitor.post.addPost', compact('divisions','districs','upazilas','unions','departments','preClasses','subjects','facultys','universitys','countrys'))->with('no', 1);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        $validator = Validator::make($input, [
            'tinsName' => 'required|string|max:255',
            'email' => 'required|max:255',
            'tutionType' => 'required|max:255',
            'sgender' => 'max:255|nullable',
            'medium' => 'required|max:255',
            'subject' => 'max:255|nullable',
            'department' => 'max:255|nullable',
            'country' => 'required|max:255',     
            'division' => 'max:255|nullable',
             'district' => 'required|max:255',
            'city' => 'required|max:255',
            'area' => 'max:255|nullable',
            'category' => 'max:255|nullable',
            'class' => 'max:255|nullable',
            'sins_name' => 'max:255',
            'tgender' => 'max:255',
            'address' => 'max:255|nullable',
            'noStudent' => 'max:255|nullable',
            'days' => 'max:255|nullable',
            'jDate' => 'max:255|nullable',
            'mobile' => 'max:255',
            'a_mobile' => 'max:255',
            'selery' => 'max:255|nullable',
            'hereAboutUs' => 'max:255|nullable',            
            's_requirment[]' => 'max:255|nullable',
            'tinsName[]' => 'max:255|nullable',
        ]);

       if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
      
        $post = Post::create($input);
         if($post->save()){
            $id=$post->id;
            // foreach ($request->post as $key => $vl){
                $data = array(
                    'post_id'=>$id,
                    'tinsName'=>$request->tinsName,
                );
                PostInst::insert($data);
        // return $this->sendResponse($post->toArray(), 'Product created successfully.');
    // }
          return redirect()->back()->with('message', 'Your Post Publish Now!');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post=Post::where('id',$id)->first();
        // return $post;
        return view('visitor.post.postView',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
