<?php

namespace App\Http\Controllers;
use \Shipu\BanglalinkSmsGateway\Facades\Banglalink;
use Illuminate\Http\Request;
use App\User;
class VerifyController extends Controller
{
      public function codeverify(){
       
       return view('auth.verify');

     }

      public function postVerify(Request $request){

       // return $request->code;
      	if($user=User::where('code',$request->code)->first()){
      		$user->active=1;
      		$user->code=null;
      		$user->save();
      		return redirect('/tutorProfile');
      		// return redirect()->back()->with('message', 'OTP not Match!');
      	}
      	else{
      		return redirect()->back()->with('message', 'OTP not Match!');
      	}
      	
      	
     }
}
