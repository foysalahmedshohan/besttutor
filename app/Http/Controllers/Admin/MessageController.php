<?php

namespace App\Http\Controllers\Admin;
use \Shipu\BanglalinkSmsGateway\Facades\Banglalink;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.message.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    $numbers = Post::select('mobile')->distinct()->get();
    //return $numbers;

    // foreach($numbers as $number) {
    //  $number= $number->mobile;
    // // $value=(rand(10,1000000)); 

    //  $sms = Banglalink::message($request->message)
    //         ->to($number)
    //         ->send();
    //          // return $sms->autoParse();
    //  }


    //  $users = [
    // '01616022669',
    // '01845736124',
    //  '01745987364'
    // ];        
     $sms = Banglalink::message($request->message,$numbers)->send(); 
    
             
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
