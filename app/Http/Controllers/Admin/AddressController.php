<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use App\Post;
use App\PostInst;
use App\Country;
use App\Division;
use App\District;
use App\Upazila;
use App\Union;
use App\Department;
use App\PreClass;
use App\Subject;
use App\Faculty;
use App\University;
use App\Tprofile;
use DB;

class AddressController extends Controller
{
     public function division(Request $request){
        $division = Division::where("country_id", $request->id)
             ->pluck("name");
             return $division;
         return response()->json($division);
    }

     public function district(Request $request){
        $district = District::where("division_id", $request->id)
             ->pluck("name");
             return $district;
         return response()->json($district);
    }

     public function upazila(Request $request){
        $district = Upazila::where("district_id", $request->id)
             ->pluck("name");
             return $district;
         return response()->json($district);
    }

     public function area(Request $request){
        $upazila = Union::where("upazilla_id", $request->id)
             ->pluck("name");
             return $upazila;
         return response()->json($upazila);
    }
}
