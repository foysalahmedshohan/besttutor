<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\PostInst;
use Validator;
use App\Tprofile;

class ManageController extends Controller
{
    public function postList(){
    	$posts=Post::orderBy('id', 'ASC')->get();
        return view('admin.post.index',compact('posts'))->with('no',1);

    }

    public function postDelete($id){
    	$post = Post::findOrFail($id);
        $post->delete();
        return redirect()->back()->withSuccess('IT SUCCESSFULLY DELETED!');   	
    }

    public function tutorList(){
    	$tutors=Tprofile::orderBy('id', 'ASC')->get();
        return view('admin.tutor.index',compact('tutors'))->with('no',1);
    	
    }

    public function tutorDelete($id){
    	return $id;
    	$tutor = Tprofile::findOrFail($id);
        $tutor->delete();
        return redirect()->back()->withSuccess('IT SUCCESSFULLY DELETED!');  
    	
    }


 
   

}
