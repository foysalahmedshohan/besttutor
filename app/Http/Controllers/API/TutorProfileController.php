<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use Validator;
use App\Post;
use App\PostInst;
use App\Country;
use App\Division;
use App\District;
use App\Upazila;
use App\Union;
use App\Department;
use App\PreClass;
use App\Subject;
use App\Faculty;
use App\University;
use App\Tprofile;
use DB;

class TutorProfileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $profiles = Tprofile::all();       
        return $this->sendResponse($profiles->toArray(), 'Profile retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $request;
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|string|max:255',
            'university' => 'required|max:255',
            'ssc_ins' => 'required|max:255',
            'faculty' => 'max:255|nullable',
            'department' => 'required|max:255',
            'emb' => 'max:255|nullable',
            'country' => 'max:255|nullable',
            'division' => 'required|max:255',     
            'district' => 'required|max:255',
            'city' => 'required|max:255',
            'area' => 'max:255|nullable',
            'hsc_ins' => 'required|max:255',
            'pre_class' => 'max:255|nullable',
            'pre_subject' => 'max:255|nullable',
            'bdate' => 'string|max:255',
            'address' => 'max:255|nullable',
            'gender' => 'max:255|nullable',
            'nid' => 'max:255|nullable',
            'selery' => 'max:255|nullable',
            'f_mobile' => 'max:255',
            'm_mobile' => 'max:255',
        ]);
         if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $profile = Post::Tprofile($input);    
        return $this->sendResponse($profile->toArray(), 'Post created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $profile =Tprofile::where('user_id',$id)->first(); 

          if (is_null($profile)) {
            $message= 'Data not Found';
            $user_id= 'null';

                    $response = [
                    'success' => false,
                    'message' => $message,
                    'user_id' => $user_id
                ];
            }
    else
    {

        $country =Country::where('id',$profile->country)->pluck('name')->first(); 
        $division =Division::where('id',$profile->division)->pluck('name')->first(); 
        $district =District::where('id',$profile->district)->pluck('name')->first(); 
        $upazila =Upazila::where('id',$profile->city)->pluck('name')->first();
        $union =Union::where('id',$profile->area)->pluck('name')->first();
        $university =University::where('id',$profile->university)->pluck('name')->first(); 
        $faculty =Faculty::where('id',$profile->faculty)->pluck('name')->first();
        $department =Department::where('id',$profile->department)->pluck('name')->first(); 
        $class =PreClass::where('id',$profile->pre_class)->pluck('name')->first(); 
        $subject =Subject::where('id',$profile->pre_subject)->pluck('name')->first(); 
        $message= 'Data Found successfully';

      $response = [
            'success' => true,
            'message' => $message,
            'profile'    => $profile,
            'country'    => $country,
            'division'    => $division,
            'district'    => $district,
            'upazila'    => $upazila,
            'union' => $union,
            'university'    => $university,
            'faculty'    => $faculty,
            'department'    => $department,
            'class'    => $class,
            'subject'    => $subject
        ];


        return response()->json($response, 200);
      }

       //  $country = DB::table('tprofiles')
       // ->leftjoin('countries', 'tprofiles.country', '=','countries.id')
       // ->leftjoin('divisions', 'tprofiles.division', '=','divisions.id')
       // ->select('countries.*', 'countries.name')->pluck('name');

       // $division = DB::table('tprofiles')
       // ->leftjoin('divisions', 'tprofiles.division', '=','divisions.id')
       // ->select('divisions.*', 'divisions.name')->pluck('name');
       // return response()->json(['country' => $country, 'division' => $division, 'profile' => $profile]);









        // $id=$id;
        // $profile =Tprofile::where('user_id',$id)->first();   
        if (is_null($profile)) {
            return $this->sendError('Profile not found.');
        }
        // return $this->sendResponse($profile->toArray(), 'Profile retrieved successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
