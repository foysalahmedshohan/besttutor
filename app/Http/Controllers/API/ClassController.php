<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PreClass;

class ClassController extends BaseController
{
	 public function class()
	 {
     $classes = PreClass::all();       
     return $this->sendResponse($classes->toArray(), 'class retrieved successfully.');
     }
}
