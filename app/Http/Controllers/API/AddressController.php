<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Country;
use App\Division;
use App\District;
use App\Upazila;
use App\Union;


class AddressController extends BaseController
{

	 public function country_show()
	 {
     $countrys = Country::get();   
     return $this->sendResponse($countrys->toArray(), 'Country retrieved successfully.'); 
     // return $country;
     }


	 public function division_show($id)
	 {
     $divisions = Division::where("country_id", $id)->get();    
     return $this->sendResponse($divisions->toArray(), 'Division retrieved successfully.');
     }

      public function district_show($id)
	 {
     $districts = District::where("id", $id)->get();  
     return $this->sendResponse($districts->toArray(), 'District retrieved successfully.');  

     }

      public function upazila_show($id)
	 {
     $upazilas = Upazila::where("id", $id)->get();
     return $this->sendResponse($upazilas->toArray(), 'Upazila retrieved successfully.');    

     }

       public function area_show($id)
	 {
     $areas = Union::where("id", $id)->get(); 
     return $this->sendResponse($areas->toArray(), 'Country retrieved successfully.');   

     }

}