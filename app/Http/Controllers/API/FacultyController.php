<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Faculty;


class FacultyController extends BaseController
{
	public function faculty()
	 {
     $faculty = Faculty::all();       
     return $this->sendResponse($faculty->toArray(), 'Faculty retrieved successfully.');
     }
}
