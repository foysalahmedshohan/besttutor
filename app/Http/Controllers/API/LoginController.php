<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User; 
use Validator;

class LoginController extends BaseController
{
    // public function login(Request $request){
    // 	return 2;
    // }

    public $successStatus = 200;
/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
      public function login(Request $request){ 
       
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user=Auth::user()->id;
            $message='User Match';
            $code=200;
            return response()->json(['user ' => $user,'code ' => $code, 'message' => $message]);


        }
        else{
            $code=0;
            $message='Not Match';
            return response()->json(['code ' => $code, 'message' => $message]);

        }

    }
}    
 


    

