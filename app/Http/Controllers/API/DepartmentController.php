<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Department;

class DepartmentController extends BaseController
{
	public function department()
	 {
     $departments = Department::all();       
     return $this->sendResponse($departments->toArray(), 'Department retrieved successfully.');
     }
}
