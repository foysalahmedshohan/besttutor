<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use Validator;
use App\Post;
use App\PostInst;
use App\Country;
use App\Division;
use App\District;
use App\Upazila;
use App\Union;
use App\Department;
use App\PreClass;
use App\Subject;
use App\Faculty;
use App\University;
use App\Tprofile;
use DB;
class PostController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $posts = Post::all();       
        return $this->sendResponse($posts->toArray(), 'Post retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input =json_decode(json_encode($request->all()),true);
        
        // $input = $request->all();
        $validator = Validator::make($input, [
            'tinsName' => 'required|string|max:255',
            'email' => 'required|max:255',
            'tutionType' => 'required|max:255',
            'sgender' => 'max:255|nullable',
            'medium' => 'required|max:255',
            'subject' => 'max:255|nullable',
            'tcourse' => 'max:255|nullable',
            'country' => 'required|max:255',     
            'division' => 'max:255|nullable',
             'district' => 'required|max:255',
            'city' => 'required|max:255',
            'area' => 'max:255|nullable',
            'tutionType' => 'required|max:255',
            'category' => 'max:255|nullable',
            'class' => 'max:255|nullable',
            'sins_name' => 'max:255',
            'tgender' => 'max:255',
            'address' => 'max:255|nullable',
            'noStudent' => 'max:255|nullable',
            'days' => 'max:255|nullable',
            'jDate' => 'max:255|nullable',
            'mobile' => 'max:255',
            'a_mobile' => 'max:255',
            'selery' => 'max:255|nullable',
            'hereAboutUs' => 'max:255|nullable',            
            's_requirment[]' => 'max:255|nullable',
            'tinsName[]' => 'max:255|nullable',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }


         $post = Post::create($input);
         if($post->save()){
            $id=$post->id;
            // foreach ($request->post as $key => $vl){
                $data = array(
                    'post_id'=>$id,
                    'tinsName'=>$request->tinsName,
                );
                PostInst::insert($data);
    
        return $this->sendResponse($post->toArray(), 'Post created successfully.');
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $post =Post::where('id',$id)->first(); 

          if (is_null($post)) {
            $message= 'Post not Found';
            $user_id= 'null';

                    $response = [
                    'success' => false,
                    'message' => $message,
                    'user_id' => $user_id
                ];
        return response()->json($response, 200);
            }
    else
    {

        $country =Country::where('id',$post->country)->pluck('name')->first(); 
        $division =Division::where('id',$post->division)->pluck('name')->first(); 
        $district =District::where('id',$post->district)->pluck('name')->first(); 
        $upazila =Upazila::where('id',$post->city)->pluck('name')->first();
        $union =Union::where('id',$post->area)->pluck('name')->first();

        // $faculty =Faculty::where('id',$post->faculty)->pluck('name')->first();
        $department =Department::where('id',$post->department)->pluck('name')->first(); 
        $class =PreClass::where('id',$post->class)->pluck('name')->first(); 
        $subject =Subject::where('id',$post->subject)->pluck('name')->first(); 
        $university=PostInst::where('post_id',$id)->pluck('tinsName')->first();
        $message= 'Data Found successfully';

      $response = [
            'success' => true,
            'message' => $message,
            'profile'    => $post,
            'country'    => $country,
            'division'    => $division,
            'district'    => $district,
            'upazila'    => $upazila,
            'union' => $union,
            'university'    => $university,
            // 'faculty'    => $faculty,
            'department'    => $department,
            'class'    => $class,
            'subject'    => $subject
        ];


        return response()->json($response, 200);
      }




        // $posts = Post::find($id);   
        // if (is_null($posts)) {
        //     return $this->sendError('Product not found.');
        // }
        // return $this->sendResponse($posts->toArray(), 'Post retrieved successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $posts = Post::find($id);   
        if (is_null($posts)) {
            return $this->sendError('Post not found.');
        }
        return $this->sendResponse($posts->toArray(), 'Post retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $post = Post::findOrFail($id);
        $post->delete();
        return $this->sendResponse($post->toArray(), 'Post deleted successfully.');
    }
}
