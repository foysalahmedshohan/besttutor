<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends BaseController
{
     public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'numeric', 'min:11'],
            'r_phone' => ['required', 'numeric', 'min:11'],
            'aboutus' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8']

        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $success['email'] =  $user->email;
        $success['phone'] =  $user->phone;
        $success['aboutus'] =  $user->aboutus;
        $success['r_phone'] =  $user->r_phone;

        return $this->sendResponse($success, 'User register successfully.');
    }
}
