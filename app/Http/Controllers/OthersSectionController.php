<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OthersSectionController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function signin()
    {
        return view('visitor.sign.signin');
    }

    public function contact()
    {
        return view('visitor.contact.contact');
    }
    public function register()
    {
        return view('visitor.sign.signup');
    }

    public function adminindex()
    {
        return view('admin.index');
    }


}
