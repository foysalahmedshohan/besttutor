<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Post;
use App\PostInst;
use App\Country;
use App\Division;
use App\District;
use App\Upazila;
use App\Union;
use App\Department;
use App\PreClass;
use App\Subject;
use App\Faculty;
use App\University;
use App\Tprofile;
use DB;


class TutorProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
       
        return view('tutorProfile.home');
    }

     public function postList()
    {
        $posts=Post::paginate(5);
        return view('tutorProfile.postList', compact('posts'))->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countrys=Country::orderBy('name', 'asc')->get();
        $divisions=Division::orderBy('name', 'asc')->get();
        $districs=District::orderBy('name', 'asc')->get();
        $upazilas=Upazila::orderBy('name', 'asc')->get();
        $unions=Union::orderBy('name', 'asc')->get();
        $departments=Department::orderBy('name', 'asc')->get();
        $preClasses=PreClass::orderBy('name', 'asc')->get();
        $subjects=Subject::orderBy('name', 'asc')->get();
        $facultys=Faculty::orderBy('name', 'asc')->get();
        $universitys=University::orderBy('name', 'asc')->get();
        $id=Auth::id();
        $profileinfo=Tprofile::where('user_id',$id)->first();

        if($profileinfo!=null){

        $value=1;
        $country =Country::where('id',$profileinfo->country)->pluck('name')->first(); 
        $division =Division::where('id',$profileinfo->division)->pluck('name')->first(); 
        $district =District::where('id',$profileinfo->district)->pluck('name')->first(); 
        $upazila =Upazila::where('id',$profileinfo->city)->pluck('name')->first();
        $union =Union::where('id',$profileinfo->area)->pluck('name')->first();

        return view('tutorProfile.addProfile', compact('country','division','district','upazila','union','id','profileinfo','value'))->with('no', 1);


        }
        else{

        $value=0;
        return view('tutorProfile.addProfile', compact('divisions','districs','upazilas','unions',
            'departments','preClasses','subjects','facultys','universitys','countrys','id','profileinfo','value'))->with('no', 1);
        }
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|string|max:255',
            'university' => 'required|max:255',
            'ssc_ins' => 'required|max:255',
            'faculty' => 'max:255|nullable',
            'department' => 'required|max:255',
            'emb' => 'max:255|nullable',
            'country' => 'max:255|nullable',
            'division' => 'required|max:255',     
            'district' => 'required|max:255',
            'city' => 'required|max:255',
            'area' => 'max:255|nullable',
            'hsc_ins' => 'required|max:255',
            'pre_class' => 'max:255|nullable',
            'pre_subject' => 'max:255|nullable',
            'bdate' => 'string|max:255',
            'address' => 'max:255|nullable',
            'gender' => 'max:255|nullable',
            'nid' => 'max:255|nullable',
            'selery' => 'max:255|nullable',
            'f_mobile' => 'max:255',
            'm_mobile' => 'max:255',
        ]);

       if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $profile = Tprofile::create($input);        
        return redirect()->back()->with('message', 'Your Profile is Complete!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
