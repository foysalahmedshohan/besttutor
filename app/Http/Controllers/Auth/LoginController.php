<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = 'tutorProfile';



       public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
       public function login_page()
    {
       return view('admin.login.login');
    }


    public function admin_index()
    {
       return view('admin.index');
    }

   
    public function login(Request $request)
    {   

         //return $request->all();

        $input = $request->all();
   
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password'])))
        {
            
            if ( (auth()->user()->active == 1) && (auth()->user()->admin_type == 1) ) {
                 //return 11111;
                
                //return redirect()->route('admin.adminLogin');
                 }

            elseif( (auth()->user()->active == 1) && (auth()->user()->admin_type == 0) )
                {  
                    //return 01;
                   // return redirect()->route('tutorProfile.index');
                  //  return redirect()->route('admin.adminLogin');
                }
        }
        else{
            return redirect()->route('login')
                ->with('error','Email-Address Or Password Are Wrong.');
        }
        
          
    }






    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }
}
